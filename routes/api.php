<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

// Route::middleware('auth:api')->get('/user', function (Request $request) {
//     return $request->user();
// });

    Route::post('login','Api\AuthController@login');
    Route::post('password/reset', 'Api\AuthController@resetPassword');
    Route::get('password/confirm/', 'Api\AuthController@confirm');
    Route::post('password/reset-password', 'Api\AuthController@reset');
    Route::post('/logout','Api\AuthController@logout');
    Route::group(['middleware' => 'auth:api'], function(){
        Route::get('logout', 'Api\AuthController@logout')->name('api.jwt.logout');
    });

    Route::group(['middleware' => 'isAdmin' ], function () {
        Route::resource('group','Api\GroupController');
        Route::resource('partner','Api\PartnerController');
        Route::resource('rank','Api\RankController');
        Route::resource('staff','Api\StaffController');
        Route::resource('skill','Api\SkillController');
        Route::get('/export-group','Export\ExportGroupController@export');
        Route::get('/export-partner','Export\ExportPartnerController@export');
        Route::resource('staff','Api\StaffController');
        Route::get('/export-staff','Export\ExportStaffController@export');
        Route::get('/export-rank','Export\ExportRankController@export');
        Route::get('/export-developer-type','Export\ExportDeveloperTypeController@export');
        Route::get('/export-skill','Export\ExportSkillController@export');
        Route::get('/common','Api\CommonApiController@commons');
        Route::resource('developer-type', 'Api\DeveloperTypeController');
        Route::post('/import-developer-type','Import\ImportDeveloperTypeController@importFile');
        Route::post('/import-group','Import\ImportGroupController@importFile');
        Route::post('/import-skill','Import\ImportSkillController@importFile');
        Route::post('/import-rank','Import\ImportRankController@importFile');
        Route::group(['middleware' => 'isSuperAdmin'], function () {
            Route::apiResource('project','Api\ProjectController');
            Route::get('/export-partner','Export\ExportPartnerController@export');
            Route::get('/export-project','Export\ExportProjectController@export');
            Route::post('/import-partner','Import\ImportPartnerController@importFile');
        });
    });



