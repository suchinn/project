<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProjectsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('projects', function (Blueprint $table) {
            $table->Increments('id')->length(10);
            $table->unsignedInteger('partner_id');
            $table->foreign('partner_id')->references('id')->on('partners')->onDelete('cascade');
            $table->integer('project_type_id')->length(10);
            $table->string('project_code')->length(10);
            $table->string('project_name');
            $table->integer('status');
            $table->dateTime('start_date');
            $table->dateTime('end_date');
            $table->integer('team_leader_id');
            $table->integer('leader_id');
            $table->integer('comtor_id');
            $table->integer('brse_id');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('projects');
    }
}
