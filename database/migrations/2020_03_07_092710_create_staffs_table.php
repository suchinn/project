<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateStaffsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('staffs', function (Blueprint $table) {
            $table->Increments('id')->length(10);
            $table->string('name');
            $table->unsignedInteger('rank_id');
            $table->foreign('rank_id')->references('id')->on('ranks')->onDelete('cascade');
            $table->unsignedInteger('position_id');
            $table->foreign('position_id')->references('id')->on('positions')->onDelete('cascade');
            $table->unsignedInteger('developer_type_id')->length(10);
            $table->foreign('developer_type_id')
            ->references('id')
            ->on('develop_types')
            ->onDelete('cascade');
            $table->unsignedInteger('group_id')->length(10);
            $table->foreign('group_id')
            ->references('id')->on('groups')->onDelete('cascade');
            $table->dateTime('experience');
            $table->text('note')->nullable();
            $table->integer('status')->length(10);
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('staffs');
    }
}
