<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Models\Position;
use Faker\Generator as Faker;

$factory->define(Position::class, function (Faker $faker) {
    return [
         'name' => $faker->name,
        // 'status' => 1,
        'description' => 'clgt',
        'created_at' => now(),
    ];
});
