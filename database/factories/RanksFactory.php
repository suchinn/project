<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Models\Rank;
use Faker\Generator as Faker;

$factory->define(Rank::class, function (Faker $faker) {
    return [
         'name' => $faker->name,
        // 'status' => 1,
        'description' => 'clgt',
        'created_at' => now(),
    ];
});
