<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */
use App\Models\User;
use Illuminate\Support\Str;
use Faker\Generator as Faker;

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| This directory should contain each of the model factory definitions for
| your application. Factories provide a convenient way to generate new
| model instances for testing / seeding your application's database.
|
*/

$factory->define(User::class, function (Faker $faker) {
    return [
        'email' => 'anhdxdev@gmail.com',
        'password' => '$2y$12$8tZW0u5wSjToxHWJzrWgfuWfiGT15gERL8AHHgNW3g/fxQ779hPue',
        'name'	=> 'ket',
        'status'=> 1,
        'role_id' => 1,
        'staff_id'	=>1,
        'created_at'=> now(),
        // 'remember_token' => Str::random(10),
    ];
});
