<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Models\Develop_type;
use Faker\Generator as Faker;

$factory->define(Develop_type::class, function (Faker $faker) {
    return [
        'name' => $faker->name,
        'status' => 1,
        'created_at' => now(),
    ];
});
