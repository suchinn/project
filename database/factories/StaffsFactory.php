<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Models\Staff;
use Faker\Generator as Faker;

$factory->define(Staff::class, function (Faker $faker) {
    return [
        'name'	=> 'Ket',
        'rank_id'=> 1,
        'position_id'=>1,
        'develop_type_id'=>1,
        'group_id'	=>1,
        'experience' => now(),
        'note'	=>null,
        'status'=> 1,
        'created_at'=> now(),
    ];
});
