<?php
namespace App;

class Json
{
    public  static function response($success = null, $code = null, $message = null, $data = []){
            return response()->json([
                'success' => $success,
                'message' => $message,
                'data'    => $data,
            ],$code);
    }
}
