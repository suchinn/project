<?php

namespace App\Notifications;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;
use Illuminate\Notifications\Notification;
use  Illuminate\Notifications\Notifiable;

class PasswordResetRequest extends Notification
{
    use Queueable,Notifiable;

    /**
     * Create a new notification instance.
     *
     * @return void
     */
    protected $token;
    protected $email;
    public function __construct($email,$token)
    {
        $this->token = $token;
        $this->email = $email;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['mail'];
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {
        $url = url(env('URL_CHANGE_PASS_FRONT_END').'?email='.$this->email.'&token='.$this->token);
        return (new MailMessage)
                    ->greeting('Reset Password!')
                    ->line('You are receiving this email because we
                     received a password reset request for your account.')
                    ->action('Reset Password', url($url))
                    ->line('If you did not request a password reset, no further action is required.');
    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        return [
            //
        ];
    }
}
