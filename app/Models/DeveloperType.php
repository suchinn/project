<?php

namespace App\Models;

use App\Models\Staff;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class DeveloperType extends Model
{
    use SoftDeletes;
    protected $table = "developer_types";
    protected $fillable = [
    	'name',
        'status',
        'description'
    ];
    public function staffs(){
    	return $this->hasMany(Staff::class);
    }
}
