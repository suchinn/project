<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Rank extends Model
{
    use SoftDeletes;
    protected $table = "ranks";
    protected $fillable = [
    	'name',
        'description',
        'status'
    ];
    public function staffs()
    {
        return $this->hasMany('App\Models\Staff');
    }
}
