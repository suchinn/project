<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
// use Illuminate\Database\Eloquent\SoftDeletes;

class Language extends Model
{
    // use SoftDeletes;
    protected $table = "languages";
    public $timestamps = false;
    protected $fillable = [
    	'language_code',
    	'staff_id',
    ];
    public function staff()
    {
        return $this->belongsTo('App\Models\Staff');
    }
}
