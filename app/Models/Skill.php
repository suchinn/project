<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Skill extends Model
{
    use SoftDeletes;
    protected $table = "skills";
    protected $fillable = [
    	'name',
    	'status',
    ];
    public function staff_skills()
    {
        return $this->hasMany('App\Models\StaffSkill');
    }
}
