<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Carbon\Carbon;

class Position extends Model
{
    use SoftDeletes;
    protected $table= "positions";
    protected $fillable= ['name','description','status'];
    public function staffs()
    {
        return $this->hasMany('App\Models\Staff');
    }
}
