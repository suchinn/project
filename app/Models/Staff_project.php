<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Staff_project extends Model
{
    use SoftDeletes;
    protected $table = "staff_projects";
    public function staff()
    {
        return $this->belongsTo('App\Models\Staff');
    }
}
