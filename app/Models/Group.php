<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
class Group extends Model
{
    use SoftDeletes;
    protected $table = "groups";
    protected $fillable = [
    	'name',
    	'description',
        'status',
        'leader_id'
    ];
    public function staffs()
    {
        return $this->hasMany('App\Models\Staff');
    }
}
