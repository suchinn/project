<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Partner extends Model
{
    use SoftDeletes;

    protected $table = "partners";
    protected $fillable = [
    	'name',
    	'description',
    	'status',
    ];

    public function projects()
    {
        return $this->hasMany('App\Models\Project');
    }
}
