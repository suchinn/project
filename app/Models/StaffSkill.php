<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class StaffSkill extends Model
{
    use SoftDeletes;
    protected $table = "staff_skills";

    protected $fillable = [
    	'skill_id',
        'staff_id',
        'status'
    ];
    public function staff()
    {
        return $this->belongsTo('App\Models\Staff');
    }
    public function skill()
    {
        return $this->belongsTo('App\Models\Skill');
    }
}
