<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Plan_project extends Model
{
    use SoftDeletes;
    protected $table = "plan_projects";
    public function project()
    {
        return $this->belongsTo('App\Models\Project');
    }
}
