<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Project extends Model
{
    use SoftDeletes;
    protected $table = "projects";
    protected $fillable = [
        'project_code','name','project_type_id','status_id','start_date','end_date',
        'brse_id','comtor_id','leader_id','partner_id','team_leader_id',
    ];
    public function plan_projects()
    {
        return $this->hasMany('App\Models\Plan_project');
    }
    public function staff_projects()
    {
        return $this->hasMany('App\Models\Staff_project');
    }
    public function partner()
    {
        return $this->belongsTo('App\Models\Partner');
    }
}
