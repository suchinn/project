<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
class Staff extends Model
{
    use SoftDeletes;
    protected $table = "staffs";
    protected $fillable = [
    	'full_name',
        'staff_code',
        'status',
        'working_type_id',
        'experience_from',
        'developer_type_id',
        'rank_id',
        'position_id',
        'group_id',
        'note'
    ];
    public function developer_type()
    {
        return $this->belongsTo('App\Models\DeveloperType');
    }
    public function staff_skill()
    {
        return $this->hasMany('App\Models\StaffSkill');
    }
    public function languages()
    {
        return $this->hasMany('App\Models\Language');
    }
    public function user()
    {
        return $this->hasOne('App\Models\User');
    }
    public function group()
    {
        return $this->belongsTo('App\Models\Group');
    }
    public function rank()
    {
        return $this->belongsTo('App\Models\Rank');
    }
    public function position()
    {
        return $this->belongsTo('App\Models\Position');
    }
}
