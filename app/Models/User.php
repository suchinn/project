<?php

namespace App\Models;

use Illuminate\Notifications\Notifiable;
use Illuminate\Support\Facades\Notification;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Tymon\JWTAuth\Contracts\JWTSubject;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Notifications\AnonymousNotifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable  implements JWTSubject
{
     use Notifiable;

    protected $table = "users";
    protected $fillable = [
    	'email',
    	'password',
    	'name',
    	'status',
    	'created_at',
    	'updated_at',
    	'deleted_at',
    ];
    public function staff()
    {
        return $this->belongsTo('App\Models\Staff');
    }
    public function role()
    {
        return $this->belongsTo('App\Models\Role');
    }
     public function getJWTIdentifier()
    {
        return $this->getKey();
    }
     public function getJWTCustomClaims()
    {
        return [];
    }

}
