<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Services\ProjectService;
use App\Http\Requests\CreateProjectRequest;
use App\Http\Requests\ShowProjectRequest;

class ProjectController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    protected $projectservice;
    public function __construct(ProjectService $projectservice)
    {
        $this->projectservice = $projectservice;
    }
    /**
     * @SWG\Get(
     *       path="/api/project",
     *       description="Get list project",
     *       tags={"Project"},
     *       security={
     *         {
     *             "TokenHeader": {}
     *         }
     *       },
     *       @SWG\Response(
     *           response=200,
     *           description="Success!",
     *     @SWG\Schema(ref="#/definitions/ResponseProject"),
     *      ),
     *       @SWG\Response(
     *           response=403,
     *           description="You can not access this server!",
     *
     *      ),
     * )
     */
    public function index(ShowProjectRequest $request)
    {
        $data = $request->all();
        $page = $request->has('page') ? $request->get('page') : 1;
        $limit = $request->has('limit') ? $request->get('limit') : 10;
        return $this->projectservice->index($data,$page,$limit);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    /**
     * @SWG\Post(
     *       path="/api/project",
     *       description="Create Your Awesome Project -_-",
     *       tags={"Project"},
     *       security={
     *         {
     *             "TokenHeader": {}
     *         }
     *       },
     *           @SWG\Parameter(
     *           name="project_code",
     *           in="formData",
     *           type="integer",
     *           required=true
     *      ),
     *       @SWG\Parameter(
     *           name="name",
     *           in="formData",
     *           type="string",
     *           required=true
     *      ),     @SWG\Parameter(
     *           name="project_type_id",
     *           in="formData",
     *           type="integer",
     *           required=true
     *      ),  @SWG\Parameter(
     *           name="status_id",
     *           in="formData",
     *           type="integer",
     *           required=true
     *      ),@SWG\Parameter(
     *           name="start_date",
     *           in="formData",
     *           type="string",
     *           required=true
     *      ),@SWG\Parameter(
     *           name="end_date",
     *           in="formData",
     *           type="string",
     *           required=true
     *      ),@SWG\Parameter(
     *           name="created_at",
     *           in="formData",
     *           type="string",
     *           required=true
     *      ),@SWG\Parameter(
     *           name="updated_at",
     *           in="formData",
     *           type="string",
     *           required=true
     *      ),@SWG\Parameter(
     *           name="brse_id",
     *           in="formData",
     *           type="integer",
     *           required=true
     *      ),@SWG\Parameter(
     *           name="comtor_id",
     *           in="formData",
     *           type="integer",
     *           required=true
     *      ),@SWG\Parameter(
     *           name="leader_id",
     *           in="formData",
     *           type="integer",
     *           required=true
     *      ),@SWG\Parameter(
     *           name="partner_id",
     *           in="formData",
     *           type="integer",
     *           required=true
     *      ),@SWG\Parameter(
     *           name="team_leader_id",
     *           in="formData",
     *           type="integer",
     *           required=true
     *      ),@SWG\Parameter(
     *           name="deleted_at",
     *           in="formData",
     *           type="string",
     *           required=false
     *      ),
     *       @SWG\Response(
     *           response=200,
     *           description="Created!",
     *     @SWG\Schema(ref="#/definitions/ResponseProject"),
     *      ),
     *       @SWG\Response(
     *           response=403,
     *           description="You can not access this server!",
     *
     *      ),
     * )
     */
    public function store(CreateProjectRequest $request)
    {
        return $this->projectservice->store($request->all());
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */

    /**
     * @SWG\Get(
     *       path="/api/project/{id}",
     *       description="Get a project",
     *       tags={"Project"},
     *       security={
     *         {
     *             "TokenHeader": {}
     *         }
     *       },
     *       @SWG\Parameter(
     *           name="id",
     *           in="formData",
     *          in="path",
     *           type="integer",
     *           required=true
     *      ),
     *       @SWG\Response(
     *           response=200,
     *           description="Success!",
     *     @SWG\Schema(ref="#/definitions/ResponseProject"),
     *      ),
     *       @SWG\Response(
     *           response=403,
     *           description="You can not access this server!",
     *
     *      ),
     * )
     */
    public function show($id)
    {
        return $this->projectservice->show($id);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */

    /**
     * @SWG\Patch(
     *       path="/api/project/{id}",
     *       description="Update Your Awesome Project -_-",
     *       tags={"Project"},
     *       security={
     *         {
     *             "TokenHeader": {}
     *         }
     *       },
     *      @SWG\Parameter(
     *           name="id",
     *           in="formData",
     *           in="path",
     *           type="integer",
     *           required=true
     *      ),
     *           @SWG\Parameter(
     *           name="project_code",
     *           in="formData",
     *           type="integer",
     *           required=true
     *      ),
     *       @SWG\Parameter(
     *           name="name",
     *           in="formData",
     *           type="string",
     *           required=true
     *      ),     @SWG\Parameter(
     *           name="project_type_id",
     *           in="formData",
     *           type="integer",
     *           required=true
     *      ),  @SWG\Parameter(
     *           name="status_id",
     *           in="formData",
     *           type="integer",
     *           required=true
     *      ),@SWG\Parameter(
     *           name="start_date",
     *           in="formData",
     *           type="string",
     *           required=true
     *      ),@SWG\Parameter(
     *           name="end_date",
     *           in="formData",
     *           type="string",
     *           required=true
     *      ),@SWG\Parameter(
     *           name="created_at",
     *           in="formData",
     *           type="string",
     *           required=true
     *      ),@SWG\Parameter(
     *           name="updated_at",
     *           in="formData",
     *           type="string",
     *           required=true
     *      ),@SWG\Parameter(
     *           name="brse_id",
     *           in="formData",
     *           type="integer",
     *           required=true
     *      ),@SWG\Parameter(
     *           name="comtor_id",
     *           in="formData",
     *           type="integer",
     *           required=true
     *      ),@SWG\Parameter(
     *           name="leader_id",
     *           in="formData",
     *           type="integer",
     *           required=true
     *      ),@SWG\Parameter(
     *           name="partner_id",
     *           in="formData",
     *           type="integer",
     *           required=true
     *      ),@SWG\Parameter(
     *           name="team_leader_id",
     *           in="formData",
     *           type="integer",
     *           required=true
     *      ),@SWG\Parameter(
     *           name="deleted_at",
     *           in="formData",
     *           type="string",
     *           required=false
     *      ),
     *       @SWG\Response(
     *           response=200,
     *           description="Created!",
     *     @SWG\Schema(ref="#/definitions/ResponseProject"),
     *      ),
     *       @SWG\Response(
     *           response=403,
     *           description="You can not access this server!",
     *
     *      ),
     * )
     */
    public function update(CreateProjectRequest $request, $id)
    {
//        dd($request->all());
        return $this->projectservice->update($request->all(),$id);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */

    /**
     * @SWG\Delete(
     *       path="/api/project/{id}",
     *       description="Remove a project",
     *       tags={"Project"},
     *       security={
     *         {
     *             "TokenHeader": {}
     *         }
     *       },
     *       @SWG\Parameter(
     *           name="id",
     *           in="formData",
     *          in="path",
     *           type="integer",
     *           required=true
     *      ),
     *       @SWG\Response(
     *           response=200,
     *           description="Success!",
     *     @SWG\Schema(ref="#/definitions/ResponseProject"),
     *      ),
     *       @SWG\Response(
     *           response=403,
     *           description="You can not access this server!",
     *
     *      ),
     * )
     */
    public function destroy($id)
    {
        return $this->projectservice->destroy($id);
    }

}
