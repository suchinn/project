<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Services\PartnerService;
use App\Http\Requests\CreatePartnerRequest;
use App\Http\Requests\UpdatePartnerRequest;

class PartnerController extends Controller
{
    private $partnerService;
    public function __construct(PartnerService $partnerService){
        $this->partnerService = $partnerService;
    }
    /**
     * @SWG\Get(
     *       path="/api/partner",
     *       description="Display partner list",
     *       tags={"Partner"},
     *       security={
     *         {
     *             "TokenHeader": {}
     *         }
     *       },
     *       @SWG\Parameter(
     *           name="name",
     *           in="query",
     *           type="string"
     *      ),
     *      @SWG\Parameter(
     *           name="description",
     *           in="query",
     *           type="string"
     *      ),
     *       @SWG\Response(
     *           response=200,
     *           description="",
     *           @SWG\Schema(ref="#/definitions/ResponseJson"),
     *      ),
     * )
     */
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        return $this->partnerService->getAllPartner($request->all());
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }
    /**
     * @SWG\Post(
     *       path="/api/partner",
     *       description="Create Partner",
     *       tags={"Partner"},
     *       security={
     *         {
     *             "TokenHeader": {}
     *         }
     *       },
     *       @SWG\Parameter(
     *           name="name",
     *           in="formData",
     *           type="string",
     *           required=true
     *      ),
     *      @SWG\Parameter(
     *           name="description",
     *           in="formData",
     *           type="string",
     *           required=true
     *      ),
     *      @SWG\Parameter(
     *           name="status",
     *           in="formData",
     *           type="string"
     *      ),
     *       @SWG\Response(
     *           response=200,
     *           description="",
     *           @SWG\Schema(ref="#/definitions/ResponseJson"),
     *      ),
     * )
     */
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(CreatePartnerRequest $request)
    {
        return $this->partnerService->createPartner($request->only(['name', 'description', 'status']));
    }
    /**
     * @SWG\Get(
     *       path="/api/partner/{id}",
     *       description="Show a partner",
     *       tags={"Partner"},
     *       security={
     *         {
     *             "TokenHeader": {}
     *         }
     *       },
     *       @SWG\Parameter(
     *           name="id",
     *           in="path",
     *           type="string",
     *           required=true
     *      ),
     *       @SWG\Response(
     *           response=200,
     *           description="",
     *           @SWG\Schema(ref="#/definitions/ResponseJson"),
     *      ),
     * )
     */
    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        return $this->partnerService->getPartner($id);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }
    /**
     * @SWG\Patch(
     *       path="/api/partner/{id}",
     *       description="Update a partner",
     *       tags={"Partner"},
     *       security={
     *         {
     *             "TokenHeader": {}
     *         }
     *       },
     *       @SWG\Parameter(
     *           name="id",
     *           in="path",
     *           type="string",
     *           required=true
     *      ),
     *      @SWG\Parameter(
     *           name="name",
     *           in="formData",
     *           type="string",
     *      ),
     *      @SWG\Parameter(
     *           name="description",
     *           in="formData",
     *           type="string",
     *      ),
     *      @SWG\Parameter(
     *           name="status",
     *           in="formData",
     *           type="string"
     *      ),
     *       @SWG\Response(
     *           response=200,
     *           description="",
     *           @SWG\Schema(ref="#/definitions/ResponseJson"),
     *      ),
     * )
     */
    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(UpdatePartnerRequest $request, $id)
    {
        return $this->partnerService->updatePartner($request->only(['name', 'description', 'status']), $id);
    }
    /**
     * @SWG\Delete(
     *       path="/api/partner/{id}",
     *       description="Delete a partner",
     *       tags={"Partner"},
     *       security={
     *         {
     *             "TokenHeader": {}
     *         }
     *       },
     *       @SWG\Parameter(
     *           name="id",
     *           in="path",
     *           type="string",
     *           required=true
     *      ),
     *       @SWG\Response(
     *           response=200,
     *           description="",
     *           @SWG\Schema(ref="#/definitions/ResponseJson"),
     *      ),
     * )
     */
    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        return $this->partnerService->removePartner($id);
    }
}
