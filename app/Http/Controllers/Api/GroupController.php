<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Requests\CreateGroupRequest;
use App\Http\Controllers\Controller;
use App\Repositories\Repository;
use App\Models\Group;
use App\Services\GroupService;
use App\Http\Requests\UpdateGroupRequest;
use App\Json;

class GroupController extends Controller
{
    private $groupService;


    public function __construct(GroupService $groupService){
        $this->groupService = $groupService;
    }
    /**
     * @SWG\Get(
     *       path="/api/group",
     *       description="Group list",
     *       tags={"Group"},
     *       security={
     *         {
     *             "TokenHeader": {}
     *         }
     *       },
     *       @SWG\Parameter(
     *           name="name",
     *           in="query",
     *           type="string"
     *      ),
     *      @SWG\Parameter(
     *           name="description",
     *           in="query",
     *           type="string"
     *      ),
     *       @SWG\Response(
     *           response=200,
     *           description="",
     *           @SWG\Schema(ref="#/definitions/ResponseJson"),
     *      ),
     * )
     */
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    /*
     * get all position
     * accepted @fillable in model
     *
     * */
    public function index(Request $request)
    {
        return $this->groupService->getAllGroup($request->all());
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }
    /**
     * @SWG\Post(
     *       path="/api/group",
     *       description="Create Group",
     *       tags={"Group"},
     *       security={
     *         {
     *             "TokenHeader": {}
     *         }
     *       },
     *       @SWG\Parameter(
     *           name="name",
     *           in="formData",
     *           type="string",
     *           required=true
     *      ),
     *      @SWG\Parameter(
     *           name="description",
     *           in="formData",
     *           type="string",
     *           required=true
     *      ),
     *      @SWG\Parameter(
     *           name="status",
     *           in="formData",
     *           type="string"
     *      ),
     *      @SWG\Parameter(
     *           name="leader_id",
     *           in="formData",
     *           type="string"
     *      ),
     *       @SWG\Response(
     *           response=200,
     *           description="",
     *           @SWG\Schema(ref="#/definitions/ResponseJson"),
     *      ),
     * )
     */
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(CreateGroupRequest $request)
    {

        return $this->groupService->store($request->only(['name','description','status', 'leader_id']));

    }
    /**
     * @SWG\Get(
     *       path="/api/group/{id}",
     *       description="Show a group",
     *       tags={"Group"},
     *       security={
     *         {
     *             "TokenHeader": {}
     *         }
     *       },
     *       @SWG\Parameter(
     *           name="id",
     *           in="path",
     *           type="string",
     *           required=true
     *      ),
     *       @SWG\Response(
     *           response=200,
     *           description="",
     *           @SWG\Schema(ref="#/definitions/ResponseJson"),
     *      ),
     * )
     */
    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        return $this->groupService->detailGroup($id);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {

    }
    /**
     * @SWG\Patch(
     *       path="/api/group/{id}",
     *       description="Update a group",
     *       tags={"Group"},
     *       security={
     *         {
     *             "TokenHeader": {}
     *         }
     *       },
     *       @SWG\Parameter(
     *           name="id",
     *           in="path",
     *           type="string",
     *           required=true
     *      ),
     *      @SWG\Parameter(
     *           name="name",
     *           in="formData",
     *           type="string",
     *      ),
     *      @SWG\Parameter(
     *           name="description",
     *           in="formData",
     *           type="string",
     *      ),
     *      @SWG\Parameter(
     *           name="status",
     *           in="formData",
     *           type="string"
     *      ),
     *      @SWG\Parameter(
     *           name="leader_id",
     *           in="formData",
     *           type="string"
     *      ),
     *       @SWG\Response(
     *           response=200,
     *           description="",
     *           @SWG\Schema(ref="#/definitions/ResponseJson"),
     *      ),
     * )
     */
    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(UpdateGroupRequest $request, $id)
    {
        $newGroup = $request->only(['name', 'description', 'status', 'leader_id']);
        return $this->groupService->updateGroup($newGroup, $id);
    }
    /**
     * @SWG\Delete(
     *       path="/api/group/{id}",
     *       description="Delete a group",
     *       tags={"Group"},
     *       security={
     *         {
     *             "TokenHeader": {}
     *         }
     *       },
     *       @SWG\Parameter(
     *           name="id",
     *           in="path",
     *           type="string",
     *           required=true
     *      ),
     *       @SWG\Response(
     *           response=200,
     *           description="",
     *           @SWG\Schema(ref="#/definitions/ResponseJson"),
     *      ),
     * )
     */
    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $data = $this->groupService->destroy($id);
        return $data;
    }
}
