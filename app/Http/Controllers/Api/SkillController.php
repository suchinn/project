<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Requests\CreateSkillRequest;
use App\Http\Requests\UpdateSkillRequest;
use App\Services\SkillService;

class SkillController extends Controller
{
    private $skillService;
    public function __construct(SkillService $skillService){
        $this->skillService = $skillService;
    }

    /**
     * @SWG\Get(
     *       path="/api/skill/",
     *       description="Get All Skill",
     *       tags={"Skill"},
     *       security={
     *         {
     *             "TokenHeader": {}
     *         }
     *       },
     *       @SWG\Response(
     *           response=200,
     *           description="",
     *           @SWG\Schema(ref="#/definitions/ResponseJson"),
     *      ),
     * )
     */
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return $this->skillService->getAllSkill();
    }

    /**
     * @SWG\Post(
     *       path="/api/skill/",
     *       description="Create a new Skill",
     *       tags={"Skill"},
     *       security={
     *         {
     *             "TokenHeader": {}
     *         }
     *       },
     *      @SWG\Parameter(
     *           name="name",
     *           in="formData",
     *           type="string",
     *           required=true
     *      ),
     *      @SWG\Parameter(
     *           name="status",
     *           in="formData",
     *           type="string",
     *      ),
     *       @SWG\Response(
     *           response=200,
     *           description="",
     *           @SWG\Schema(ref="#/definitions/ResponseJson"),
     *      ),
     * )
     */
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(CreateSkillRequest $request)
    {
        return $this->skillService->createSkill($request->only(['name', 'status']));
    }

    /**
     * @SWG\Get(
     *       path="/api/skill/{id}",
     *       description="Get a Skill",
     *       tags={"Skill"},
     *       security={
     *         {
     *             "TokenHeader": {}
     *         }
     *       },
     *       @SWG\Parameter(
     *           name="id",
     *           in="path",
     *           type="integer",
     *           required=true
     *      ),
     *       @SWG\Response(
     *           response=200,
     *           description="",
     *           @SWG\Schema(ref="#/definitions/ResponseJson"),
     *      ),
     * )
     */
    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        return $this->skillService->getSkill($id);
    }

    /**
     * @SWG\Patch(
     *       path="/api/skill/{id}",
     *       description="Update the Skill",
     *       tags={"Skill"},
     *       security={
     *         {
     *             "TokenHeader": {}
     *         }
     *       },
     *       @SWG\Parameter(
     *           name="id",
     *           in="path",
     *           type="integer",
     *           required=true
     *      ),
     *      @SWG\Parameter(
     *           name="name",
     *           in="formData",
     *           type="string",
     *      ),
     *      @SWG\Parameter(
     *           name="status",
     *           in="formData",
     *           type="string",
     *      ),
     *       @SWG\Response(
     *           response=200,
     *           description="",
     *           @SWG\Schema(ref="#/definitions/ResponseJson"),
     *      ),
     * )
     */
    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(UpdateSkillRequest $request, $id)
    {
        return $this->skillService->updateSkill($request->only(['name', 'status']), $id);
    }

    /**
     * @SWG\Delete(
     *       path="/api/skill/{id}",
     *       description="Delete the Skill",
     *       tags={"Skill"},
     *       security={
     *         {
     *             "TokenHeader": {}
     *         }
     *       },
     *       @SWG\Parameter(
     *           name="id",
     *           in="path",
     *           type="integer",
     *           required=true
     *      ),
     *       @SWG\Response(
     *           response=200,
     *           description="",
     *           @SWG\Schema(ref="#/definitions/ResponseJson"),
     *      ),
     * )
     */
    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        return $this->skillService->removeSkill($id);
    }
}
