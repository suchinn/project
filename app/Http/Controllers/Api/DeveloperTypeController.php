<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Requests\CreateDeveloperTypeRequest;
use App\Http\Requests\UpdateDeveloperTypeRequest;
use App\Services\DeveloperTypeService;

class DeveloperTypeController extends Controller
{
    private $devTypeService;
    public function __construct(DeveloperTypeService $devTypeService){
        $this->devTypeService = $devTypeService;
    }

    /**
     * @SWG\Get(
     *       path="/api/developer-type/",
     *       description="Get All developer type",
     *       tags={"Developer Type"},
     *       security={
     *         {
     *             "TokenHeader": {}
     *         }
     *       },
     *       @SWG\Response(
     *           response=200,
     *           description="",
     *           @SWG\Schema(ref="#/definitions/ResponseJson"),
     *      ),
     * )
     */
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return $this->devTypeService->getAllDeveloperType();
    }

    /**
     * @SWG\Post(
     *       path="/api/developer-type/",
     *       description="Create a new Developer Type",
     *       tags={"Developer Type"},
     *       security={
     *         {
     *             "TokenHeader": {}
     *         }
     *       },
     *      @SWG\Parameter(
     *           name="name",
     *           in="formData",
     *           type="string",
     *           required=true
     *      ),
     *      @SWG\Parameter(
     *           name="status",
     *           in="formData",
     *           type="string",
     *      ),
     *       @SWG\Response(
     *           response=200,
     *           description="",
     *           @SWG\Schema(ref="#/definitions/ResponseJson"),
     *      ),
     * )
     */
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(CreateDeveloperTypeRequest $request)
    {
        return $this->devTypeService->createDeveloperType($request->only(['name', 'status']));
    }

    /**
     * @SWG\Get(
     *       path="/api/developer-type/{id}",
     *       description="Get a developer type",
     *       tags={"Developer Type"},
     *       security={
     *         {
     *             "TokenHeader": {}
     *         }
     *       },
     *       @SWG\Parameter(
     *           name="id",
     *           in="path",
     *           type="integer",
     *           required=true
     *      ),
     *       @SWG\Response(
     *           response=200,
     *           description="",
     *           @SWG\Schema(ref="#/definitions/ResponseJson"),
     *      ),
     * )
     */
    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        return $this->devTypeService->getDeveloperType($id);
    }

    /**
     * @SWG\Patch(
     *       path="/api/developer-type/{id}",
     *       description="Update the developer type",
     *       tags={"Developer Type"},
     *       security={
     *         {
     *             "TokenHeader": {}
     *         }
     *       },
     *       @SWG\Parameter(
     *           name="id",
     *           in="path",
     *           type="integer",
     *           required=true
     *      ),
     *      @SWG\Parameter(
     *           name="name",
     *           in="formData",
     *           type="string",
     *      ),
     *      @SWG\Parameter(
     *           name="status",
     *           in="formData",
     *           type="string",
     *      ),
     *       @SWG\Response(
     *           response=200,
     *           description="",
     *           @SWG\Schema(ref="#/definitions/ResponseJson"),
     *      ),
     * )
     */
    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(UpdateDeveloperTypeRequest $request, $id)
    {
        return $this->devTypeService->updateDeveloperType($request->only(['name', 'status']), $id);
    }

    /**
     * @SWG\Delete(
     *       path="/api/developer-type/{id}",
     *       description="Delete the developer type",
     *       tags={"Developer Type"},
     *       security={
     *         {
     *             "TokenHeader": {}
     *         }
     *       },
     *       @SWG\Parameter(
     *           name="id",
     *           in="path",
     *           type="integer",
     *           required=true
     *      ),
     *       @SWG\Response(
     *           response=200,
     *           description="",
     *           @SWG\Schema(ref="#/definitions/ResponseJson"),
     *      ),
     * )
     */
    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        return $this->devTypeService->removeDeveloperType($id);
    }
}
