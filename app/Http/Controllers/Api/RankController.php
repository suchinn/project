<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Requests\CreateRankRequest;
use App\Http\Controllers\Controller;
use App\Repositories\Repository;
use App\Models\Rank;
use App\Services\RankService;
use App\Http\Requests\UpdateRankRequest;
use App\Json;

class RankController extends Controller
{
    private $rankService;

    public function __construct(RankService $rankService){
        $this->rankService = $rankService;
    }
    /**
     * @SWG\Get(
     *       path="/api/rank/",
     *       description="Get All Rank",
     *       tags={"Rank"},
     *       security={
     *         {
     *             "TokenHeader": {}
     *         }
     *       },
     *      @SWG\Parameter(
     *           name="name",
     *           in="query",
     *           type="string",
     *      ),
     *       @SWG\Parameter(
     *           name="description",
     *           in="query",
     *           type="string"
     *      ),
     *       @SWG\Response(
     *           response=200,
     *           description="",
     *           @SWG\Schema(ref="#/definitions/ResponseRankList"),
     *      ),
     * )
     */
    public function index(Request $request){
        return $this->rankService->getAllRank($request->all());
    }
    /**
     * @SWG\Post(
     *       path="/api/rank/",
     *       description="Create a new Rank",
     *       tags={"Rank"},
     *       security={
     *         {
     *             "TokenHeader": {}
     *         }
     *       },
     *      @SWG\Parameter(
     *           name="name",
     *           in="formData",
     *           type="string",
     *           required=true
     *      ),
     *       @SWG\Parameter(
     *           name="description",
     *           in="formData",
     *           type="string",
     *      ),
     *      @SWG\Parameter(
     *           name="status",
     *           in="formData",
     *           type="string"
     *      ),
     *       @SWG\Response(
     *           response=200,
     *           description="",
     *           @SWG\Schema(ref="#/definitions/ResponseRank"),
     *      ),
     * )
     */
    public function store(CreateRankRequest $request){
        return $this->rankService->store($request->only(['name','description','status']));
    }

    /**
     * @SWG\Patch(
     *       path="/api/rank/{id}",
     *       description="Update a Rank",
     *       tags={"Rank"},
     *       security={
     *         {
     *             "TokenHeader": {}
     *         }
     *       },
     *      @SWG\Parameter(
     *           name="id",
     *           in="path",
     *           type="string",
     *           required=true
     *      ),
     *      @SWG\Parameter(
     *           name="name",
     *           in="formData",
     *           type="string",
     *      ),
     *       @SWG\Parameter(
     *           name="description",
     *           in="formData",
     *           type="string",
     *      ),
     *      @SWG\Parameter(
     *           name="status",
     *           in="formData",
     *           type="string"
     *      ),
     *       @SWG\Response(
     *           response=200,
     *           description="",
     *           @SWG\Schema(ref="#/definitions/ResponseRank"),
     *      ),
     * )
     */

    public function update(UpdateRankRequest $request,$id){
        $newRank = $request->only(['name','description','status']);
        return $this->rankService->update($newRank, $id);
    }

    /**
     * @SWG\Delete(
     *       path="/api/rank/{id}",
     *       description="Delete a Rank",
     *       tags={"Rank"},
     *       security={
     *         {
     *             "TokenHeader": {}
     *         }
     *       },
     *      @SWG\Parameter(
     *           name="id",
     *           in="path",
     *           type="string",
     *           required=true
     *      ),
     *       @SWG\Response(
     *           response=204,
     *           description="",
     *      ),
     * )
     */

    public function destroy($id){
        return $this->rankService->destroy($id);
    }
    
    /**
     * @SWG\Get(
     *       path="/api/rank/{id}",
     *       description="Update a Rank",
     *       tags={"Rank"},
     *       security={
     *         {
     *             "TokenHeader": {}
     *         }
     *       },
     *      @SWG\Parameter(
     *           name="id",
     *           in="path",
     *           type="string",
     *           required=true
     *      ),
     *       @SWG\Response(
     *           response=204,
     *           description="",
     *           @SWG\Schema(ref="#/definitions/ResponseRank"),
     *      ),
     * )
     */
     
    public function show($id){
        return $this->rankService->show($id);
    }
}
