<?php

namespace App\Http\Controllers\Api;

use App\Http\Requests\CreatePositionRequest;
use App\Json;
use Illuminate\Http\Request;
use App\Http\Requests\updatePositionRequest;
use App\Http\Controllers\Controller;
use App\Services\PositionService;

class PositionController extends Controller
{
    protected  $position;
    public function __construct(PositionService $positionService)
    {
        $this->position = $positionService;
    }
    /**
     * @SWG\Post(
     *       path="/api/position",
     *       description="Create a new Position",
     *       tags={"Position"},
     *       security={
     *         {
     *             "TokenHeader": {}
     *         }
     *       },
     *       @SWG\Parameter(
     *           name="name",
     *           in="formData",
     *           type="string",
     *           required=true
     *      ),
     *       @SWG\Parameter(
     *           name="description",
     *           in="formData",
     *           type="string"
     *      ),
     *      @SWG\Parameter(
     *           name="status",
     *           in="formData",
     *           type="string"
     *      ),
     *       @SWG\Response(
     *           response=200,
     *           description="",
     *           @SWG\Schema(ref="#/definitions/ResponsePosition"),
     *      ),
     * )
     */

    /*
     * add new position
     * accepted Request name, description, status
     * */
    public function store(CreatePositionRequest $request)
    {
        return $this->position->store($request->only(['name','description','status']));
    }
    /**
     * @SWG\Delete(
     *       path="/api/position/{id}",
     *       description="Delete a Position",
     *       tags={"Position"},
     *       security={
     *         {
     *             "TokenHeader": {}
     *         }
     *       },
     *       @SWG\Parameter(
     *           name="id",
     *           in="path",
     *           type="integer",
     *           required=true
     *      ),
     *       @SWG\Response(
     *           response=204,
     *           description="",
     *      ),
     * )
     */

    /*
     * destroy position
     * @param $id
     * */
    public function destroy($id)
    {
        $data = $this->position->delete($id);
        return $data;
    }
    /**
     * @SWG\Get(
     *       path="/api/position/{id}",
     *       description="Get a Position",
     *       tags={"Position"},
     *       security={
     *         {
     *             "TokenHeader": {}
     *         }
     *       },
     *       @SWG\Parameter(
     *           name="id",
     *           in="path",
     *           type="integer",
     *           required=true
     *      ),
     *       @SWG\Response(
     *           response=200,
     *           description="",
     *           @SWG\Schema(ref="#/definitions/ResponsePosition"),
     *      ),
     * )
     */

    public function show($id)
    {
        return $this->position->show($id);
    }
    /**
     * @SWG\Patch(
     *       path="/api/position/{id}",
     *       description="Update a Position",
     *       tags={"Position"},
     *       security={
     *         {
     *             "TokenHeader": {}
     *         }
     *       },
     *       @SWG\Parameter(
     *           name="id",
     *           in="path",
     *           type="integer",
     *           required=true
     *      ),
     *      @SWG\Parameter(
     *           name="name",
     *           in="formData",
     *           type="string",
     *      ),
     *       @SWG\Parameter(
     *           name="description",
     *           in="formData",
     *           type="string"
     *      ),
     *      @SWG\Parameter(
     *           name="status",
     *           in="formData",
     *           type="string"
     *      ),
     *       @SWG\Response(
     *           response=200,
     *           description="",
     *           @SWG\Schema(ref="#/definitions/ResponsePosition"),
     *      ),
     * )
     */

    /*
     * update position
     * @param $id
     * accepted Request name, description, status
     * */
    public function update(updatePositionRequest $request,$id)
    {
        $data = $request->only(['name','description','status']);
        if(!$data){
            return Json::response(false,HTTP_INVALID,trans('errors.invalid'));
        }
        return $this->position->update($data,$id);
    }
    /**
     * @SWG\Get(
     *       path="/api/position/",
     *       description="Get a Position",
     *       tags={"Position"},
     *       security={
     *         {
     *             "TokenHeader": {}
     *         }
     *       },
     *      @SWG\Parameter(
     *           name="name",
     *           in="query",
     *           type="string",
     *      ),
     *       @SWG\Parameter(
     *           name="description",
     *           in="query",
     *           type="string"
     *      ),
     *       @SWG\Response(
     *           response=200,
     *           description="",
     *           @SWG\Schema(ref="#/definitions/ResponsePositionList"),
     *      ),
     * )
     */

    /*
     * show all position in project
     *
     * */
    public function index(Request $request)
    {

        return $this->position->index($request->all());
    }
}
