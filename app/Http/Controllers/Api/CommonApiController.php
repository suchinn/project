<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Services\CommonApiService;

class CommonApiController extends Controller
{
    protected $common;
    public function __construct(CommonApiService $common)
    {
        $this->common = $common;
    }
    /**
     * @SWG\Get(
     *       path="/api/common",
     *       description="Show staff list",
     *       tags={"Common"},
     *       security={
     *         {
     *             "TokenHeader": {}
     *         }
     *       },
     *       @SWG\Response(
     *           response=200,
     *           description="",
     *           @SWG\Schema(ref="#/definitions/Common"),
     *      ),
     * )
     */
    public function commons()
    {
        return $this->common->common();
    }
}
