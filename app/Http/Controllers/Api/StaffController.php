<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Requests\CreateStaffRequest;
use App\Services\StaffService;
use App\Http\Requests\StaffListRequest;
use App\Http\Requests\UpdateStaffRequest;

class StaffController extends Controller
{
    private $staffService;
    public function __construct(StaffService $staffService){
        $this->staffService = $staffService;
    }
    /**
     * @SWG\Get(
     *       path="/api/staff",
     *       description="Show staff list",
     *       tags={"Staff"},
     *       security={
     *         {
     *             "TokenHeader": {}
     *         }
     *       },
     *       @SWG\Parameter(
     *           name="full_name",
     *           in="query",
     *           type="string"
     *      ),
     *
     *       @SWG\Response(
     *           response=200,
     *           description="",
     *           @SWG\Schema(ref="#/definitions/StaffList"),
     *      ),
     * )
     */
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(StaffListRequest $request)
    {
        return $this->staffService->getAllStaff($request->all());
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }
    /**
     * @SWG\Post(
     *       path="/api/staff",
     *       description="Create staff",
     *       tags={"Staff"},
     *       security={
     *         {
     *             "TokenHeader": {}
     *         }
     *       },
     *       @SWG\Parameter(
     *          name="body",
     *          in="body",
     *          required=true,
     *
     *          @SWG\Schema(ref="#/definitions/InputStaff")
     *     ),
     *       @SWG\Response(
     *           response=200,
     *           description="",
     *           @SWG\Schema(ref="#/definitions/AStaff"),
     *      ),
     * )
     */
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(CreateStaffRequest $request)
    {
        return $this->staffService->createStaff($request->all());
    }
     /**
     * @SWG\Get(
     *       path="/api/staff/{id}",
     *       description="Show staff list",
     *       tags={"Staff"},
     *       security={
     *         {
     *             "TokenHeader": {}
     *         }
     *       },
     *       @SWG\Parameter(
     *           name="id",
     *           in="path",
     *           type="integer",
     *           required=true
     *      ),
     *
     *       @SWG\Response(
     *           response=200,
     *           description="",
     *           @SWG\Schema(ref="#/definitions/AStaff"),
     *      ),
     * )
     */
    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
       return $this->staffService->show($id);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }
    /**
     * @SWG\Patch(
     *       path="/api/staff/{id}",
     *       description="Update staff list",
     *       tags={"Staff"},
     *       security={
     *         {
     *             "TokenHeader": {}
     *         }
     *       },
     *       @SWG\Parameter(
     *           name="id",
     *           in="path",
     *           type="integer",
     *           required=true
     *      ),
     *      @SWG\Parameter(
     *          name="body",
     *          in="body",
     *          required=true,
     *          @SWG\Schema(ref="#/definitions/InputStaff")
     *     ),
     *       @SWG\Response(
     *           response=200,
     *           description="",
     *           @SWG\Schema(ref="#/definitions/AStaff"),
     *      ),
     * )
     */
    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(UpdateStaffRequest $request, $id)
    {
        return $this->staffService->updateStaff($request->all(), $id);
    }
    /**
     * @SWG\Delete(
     *       path="/api/staff/{id}",
     *       description="Remove staff",
     *       tags={"Staff"},
     *       security={
     *         {
     *             "TokenHeader": {}
     *         }
     *       },
     *       @SWG\Parameter(
     *           name="id",
     *           in="path",
     *           type="integer",
     *           required=true
     *      ),
     *
     *       @SWG\Response(
     *           response=204,
     *           description="",
     *      ),
     * )
     */
    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        return $this->staffService->removeStaff($id);
    }
}
