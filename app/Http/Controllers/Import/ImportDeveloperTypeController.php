<?php

namespace App\Http\Controllers\Import;

use App\Json;
use Illuminate\Http\Request;
use App\Http\Requests\ImportFileRequest;
use App\Imports\DeveloperTypeImport;
use App\Http\Controllers\Controller;
use Excel;

class ImportDeveloperTypeController extends Controller
{
    /**
     * @SWG\Post(
     *       path="/api/import-developer-type",
     *       description="import developer type list",
     *       tags={"Developer Type"},
     *       security={
     *         {
     *             "TokenHeader": {}
     *         }
     *       },
     *       @SWG\Response(
     *           response=200,
     *           description="Success!",
     *      ),
     *      @SWG\Parameter(
     *           name="description",
     *           in="formData",
     *           type="file"
     *      ),
     * )
     */
    public function importFile(ImportFileRequest $request)
    {
        $dateTime = date('Ymd-His');
        $file = $request->file('file');
        $fileName = $dateTime. '-'.$file->getClientOriginalName();
        $savePatch = public_path('/upload/');
        $file->move($savePatch,$fileName);
        try{
           $data = Excel::import(new DeveloperTypeImport, $savePatch.$fileName);
        } catch (\Exception $exception){
            return Json::response(false,HTTP_BAD_GATE,trans('errors.server_busy'));
        }
    }
}
