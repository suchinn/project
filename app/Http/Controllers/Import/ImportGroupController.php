<?php

namespace App\Http\Controllers\Import;

use App\Http\Requests\ImportFileRequest;
use App\Imports\GroupImport;
use App\Json;
use Excel;
use App\Http\Controllers\Controller;

class ImportGroupController extends Controller
{
    /**
     * @SWG\Post(
     *       path="/api/import-group",
     *       description="import group list",
     *       tags={"Group"},
     *       security={
     *         {
     *             "TokenHeader": {}
     *         }
     *       },
     *       @SWG\Response(
     *           response=200,
     *           description="Success!",
     *      ),
     *       @SWG\Parameter(
     *           name="description",
     *           in="formData",
     *           type="file"
     *      ),
     * )
     */
    public function importFile(ImportFileRequest $request)
    {
        $dateTime = date('Ymd-His');
        $file = $request->file('file');
        $fileName = $dateTime. '-'.$file->getClientOriginalName();
        $savePatch = public_path('/upload/');
        $file->move($savePatch,$fileName);
        try{
            $data = Excel::import(new GroupImport, $savePatch.$fileName);
            return Json::response(true,HTTP_OK,trans('errors.success'));
        } catch (\Exception $exception){
            return Json::response(false,HTTP_BAD_GATE,trans('errors.server_busy'));
        }
    }
}
