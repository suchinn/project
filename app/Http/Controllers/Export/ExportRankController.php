<?php

namespace App\Http\Controllers\Export;

use App\Models\Rank;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Maatwebsite\Excel\Concerns\Exportable;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Excel;

class ExportRankController extends Controller implements FromCollection, WithHeadings
{
    use Exportable;
    /**
     * @SWG\Get(
     *       path="/api/export-rank",
     *       description="export rank list",
     *       tags={"Rank"},
     *       security={
     *         {
     *             "TokenHeader": {}
     *         }
     *       },
     *       @SWG\Response(
     *           response=200,
     *           description="",
     *      ),
     * )
     */
    public function collection()
    {
        $order = [];
        $rank = Rank::all();
        foreach ($rank as $row) {
            $order[] = array(
                '0' => $row->id,
                '1' => $row->name,
                '2' =>(!empty($row->description))?$row->description:null,
                '3' => $row->status,
                '4' => $row->created_at,
                '5' => $row->updated_at,
                '6' => $row->deleted_at,
            );
        }

        return (collect($order));
    }
    public function headings(): array
    {

        return [
            'id',
            'Tên',
            'Mô tả',
            'trạng thái',
            'Ngày khởi tạo',
            'Ngày cập nhật',
            'Ngày Xóa',
        ];
    }
    public function export(){
        return Excel::download(new ExportRankController(), 'rank.xlsx');
    }
}
