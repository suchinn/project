<?php

namespace App\Http\Controllers\Export;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Excel;
use App\Models\Partner;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\Exportable;
use Maatwebsite\Excel\Concerns\WithHeadings;

class ExportPartnerController extends Controller implements FromCollection, WithHeadings
{
    use Exportable;
    /**
     * @SWG\Get(
     *       path="/api/export-partner",
     *       description="export partner list",
     *       tags={"Partner"},
     *       security={
     *         {
     *             "TokenHeader": {}
     *         }
     *       },
     *       @SWG\Response(
     *           response=200,
     *           description="",
     *      ),
     * )
     */
    public function collection()
    {
        $order = [];
        $partner = Partner::all();
        foreach ($partner as $row) {
            $order[] = array(
                '0' => $row->id,
                '1' =>(!empty($row->name))?$row->name:null,
                '2' =>(!empty($row->description))?$row->description:null,
                '3' => $row->status,
                '4' => $row->created_at,
                '5' => $row->updated_at,
                '6' => $row->deleted_at,
            );
        }
        return (collect($partner));
    }
    public function headings(): array
    {
        return [
            'id',
            'Tên',
            'Mô tả',
            'trạng thái',
            'Ngày khởi tạo',
            'Ngày cập nhật',
            'Ngày Xóa',
        ];
    }
    public function export()
    {
        return Excel::download(new ExportPartnerController(), 'partner.xlsx');
    }

}
