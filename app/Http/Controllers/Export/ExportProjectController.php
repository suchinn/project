<?php

namespace App\Http\Controllers\Export;

use App\Models\Project;
use App\Models\Staff;
use App\Models\Partner;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Maatwebsite\Excel\Concerns\Exportable;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Excel;

class ExportProjectController extends Controller  implements FromCollection, WithHeadings
{
    use Exportable;
    /**
     * @SWG\Get(
     *       path="/api/export-project",
     *       description="export developer type list",
     *       tags={"Project"},
     *       security={
     *         {
     *             "TokenHeader": {}
     *         }
     *       },
     *       @SWG\Response(
     *           response=200,
     *           description="",
     *      ),
     * )
     */
    public function collection()
    {
        $order = [];
        $project = Project::all();
        foreach ($project as $row) {
            $order[] = array(
                '0' => $row->id,
                '1' => $row->project_code,
                '2' => $row->name,
                '3' => (!empty($row->project_type_id))?PROJECT_TYPE[$row->project_type_id]:null,
                '4' =>(!empty($row->status_id))?
                    (array_key_exists($row->status_id,PROJECT_STATUS)==true)?PROJECT_STATUS[$row->status_id]:null:null,
                '5' =>(!empty($row->start_date))?$row->start_date:null,
                '6' =>(!empty($row->end_date))?$row->end_date:null,
                '7' =>(!empty($row->brse_id))? $staff= Staff::where('id',$row->brse_id)->first()->full_name:null,
                '8' =>(!empty($row->comtor_id))? $staff= Staff::where('id',$row->comtor_id)->first()->full_name:null,
                '9' =>(!empty($row->leader_id))? $staff= Staff::where('id',$row->leader_id)->first()->full_name:null,
                '10' =>(!empty($row->partner_id))?
                    ($staff= Partner::where('id',$row->partner_id)->first()->name)?($staff= Partner::where('id',$row->partner_id)->first()->name)
                        :null :null,
                '11' =>(!empty($row->team_leader_id))? ($staff= Staff::where('id',$row->team_leader_id)->first()->full_name)?
                    ($staff= Staff::where('id',$row->team_leader_id)->first()->full_name):null:null,
                '12' => $row->created_at,
                '13' => $row->updated_at,
                '14' => $row->deleted_at,
            );
        }
        return (collect($order));
    }
    public function headings(): array
    {
        return [
            'id',
            'project code',
            'Tên project',
            'Loại Project',
            'Trạng thái của project',
            'Thời gian bắt đầu dự án',
            'Thời gian kết thúc dự án',
            'Brse',
            'Comtor',
            'Leader',
            'Partner',
            'Team leader',
            'Ngày tạo project',
            'Ngày cập nhật project',
            'Ngày xóa project',
        ];
    }
    public function export(){
        return Excel::download(new ExportProjectController(), 'project.xlsx');
    }
}
