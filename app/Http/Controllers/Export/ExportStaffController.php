<?php
namespace App\Http\Controllers\Export;

use App\Http\Controllers\Controller;
use Excel;
use App\Models\Staff;
use App\Models\Position;
use App\Models\Group;
use App\Models\Rank;
use App\Models\DeveloperType;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\Exportable;
use Maatwebsite\Excel\Concerns\WithHeadings;

class ExportStaffController extends Controller implements FromCollection, WithHeadings
{
    use Exportable;

    /**
     * @SWG\Get(
     *       path="/api/export-staff",
     *       description="export staff list",
     *       tags={"Staff"},
     *       security={
     *         {
     *             "TokenHeader": {}
     *         }
     *       },
     *       @SWG\Response(
     *           response=200,
     *           description="",
     *      ),
     * )
     */
    public function collection()
    {
        $order = [];
        $staff = Staff::all();
        // dd($staff->group);
        foreach ($staff as $row) {
            $order[] = array(
                '0' => $row->id,
                '1' => $row->full_name,
                '2' => $row->note,
                '3' => $row->staff_code,
                '4' =>(!empty($row->experience_from))?$row->experience_from:null,
                '5' => (array_key_exists($row->working_type_id,WORK_TYPE)==true)? WORK_TYPE[$row->working_type_id]:null,
                '6' =>(!empty($row->developer_type))?$row->developer_type->name:null,
                '7' =>(!empty($row->group))?$row->group->name:null,
                '9' =>(!empty($row->rank))?$row->rank->name:null,
                '10' => $row->status,
                '11' => $row->created_at,
                '12' => $row->updated_at,
                '13' => $row->deleted_at,
            );

        }
        return (collect($order));
    }

    public function headings(): array
    {
        return [
            'id',
            'Họ và tên',
            'Lý do nghỉ việc',
            'Mã nhân viên',
            'Bắt đầu làm việc ở công ty',
            'Thời gian làm (Full time hoặc partime) ',
            'Mô tả công việc',
            'Thuộc group nào',
            'Thứ hạng trong CTY',
            'Trạng thái',
            'Ngày tạo ',
            'Ngày cập nhật',
            'Ngày Xóa',
        ];
    }

    public function export()
    {
        return Excel::download(new ExportStaffController(), 'staff.xlsx');
    }

}
