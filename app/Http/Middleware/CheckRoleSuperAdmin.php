<?php

namespace App\Http\Middleware;
use Illuminate\Support\Facades\Auth;
use Closure;

class CheckRoleSuperAdmin
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        try{
            if(isSuperAdmin()){
                return $next($request);
            }
            else {
                return responseHelper([
                    'status'   =>false,
                    'message'   => trans('errors.forbidden')
                ], HTTP_FORBIDDEN);
            }
        }catch(\Exception $e){
            return responseHelper([
                'status'   =>false,
                'message'   => trans('errors.forbidden')
            ], HTTP_FORBIDDEN);
        }
        
    }

}
