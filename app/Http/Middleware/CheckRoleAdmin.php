<?php

namespace App\Http\Middleware;

use Closure;

class CheckRoleAdmin
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        try{
            if(isAdmin() || isSuperAdmin()){
                return $next($request);
            }else
            {
                return responseHelper([
                    'status' => false,
                    'message' => trans('errors.forbidden')
                ], HTTP_FORBIDDEN);
            } 
  

        }catch(\Exception $e){
            return responseHelper([
                'status' => false,
                'message' => trans('errors.forbidden')
            ], HTTP_FORBIDDEN);
        }
    }
}
