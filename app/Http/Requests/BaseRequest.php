<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Http\Request;
use Illuminate\Contracts\Validation\Validator;
use Illuminate\Validation\ValidationException;
use Illuminate\Http\Exceptions\HttpResponseException;

class BaseRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    protected function baseFailedValidation(Validator $validator)
    {
        $errors = (new ValidationException($validator))->errors();
        if (empty($errors)) {
            return;
        }
        $messageError = '';
        $messageErrorArray = [];
        foreach ($errors as $key => $error) {
            array_push($messageErrorArray, array_shift($error));
        }
        $messageErrorArray = array_unique($messageErrorArray);
        $messageError = implode(" ", $messageErrorArray);
        throw new HttpResponseException(
            responseHelper([
                'status' => false,
                'message' => $messageError,
            ], HTTP_INVALID)
        );
    }
}
