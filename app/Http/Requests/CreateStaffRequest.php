<?php

namespace App\Http\Requests;

use App\Http\Requests\BaseRequest;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Http\Request;
use Illuminate\Contracts\Validation\Validator;
use Illuminate\Validation\ValidationException;
use Illuminate\Http\Exceptions\HttpResponseException;

class CreateStaffRequest extends BaseRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $langCodes = array_keys(LANGUAGE);
        $workTypeIDs = array_keys(WORK_TYPE);
        $positionIDs = array_keys(POSITION);
        return [
            'full_name'  => 'required|max:255',
            'staff_code' => 'required|regex:/^\d{4}$/|unique:staffs',      // 0001
            'status' => 'regex:/^[1-3]$/',
            'skill_id' => 'array',
            'experience_from' => 'date_format:d/m/Y',
            'group_id' => 'integer|exists:groups,id,deleted_at,NULL',
            'rank_id' => 'integer|exists:ranks,id,deleted_at,NULL',
            'position_id' => 'in:'.implode(",", $positionIDs),
            'developer_type_id' => 'integer|exists:developer_types,id,deleted_at,NULL',
            'working_type_id' => 'in:'.implode(",", $workTypeIDs),
            'language_code' => 'array',
            'language_code.*' => 'in:'.implode(",", $langCodes),
            'skill_id.*' => 'integer|exists:skills,id,deleted_at,NULL' 
        ];
    }

    public function messages(){
        return [
            'working_type_id.regex' => trans('validation.custom.working_type_id_regex'),
            'status.regex' => trans('validation.custom.staff_status_regex'),
            'experience_from.regex' => trans('validation.custom.experience_from_regex'),
            'skill_id.*.integer' => trans('validation.custom.skill_id_int'),
            'skill_id.*.exists' => trans('validation.custom.skill_id_exists'),
            'language_code.*.in' => trans('validation.custom.language_code_in'),
        ];
    }
    protected function failedValidation(Validator $validator)
    {
        $this->baseFailedValidation($validator);
    }
}
