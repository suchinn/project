<?php

namespace App\Http\Requests;

use App\Http\Requests\BaseRequest;
use Illuminate\Contracts\Validation\Validator;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\ValidationException;
use Illuminate\Http\Exceptions\HttpResponseException;

class CreateProjectRequest extends BaseRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'project_code'  =>'required',
            'name'          => 'required',
            'project_type_id'=> 'required',
            'status_id'     => 'required',
            'start_date'    => 'required|date|before:end_date',
            'end_date'      => 'required|date|after:start_date',
            'brse_id'       => 'required',
            'comtor_id'     => 'required',
            'leader_id'     => 'required',
            'partner_id'    => 'required',
            'team_leader_id'    =>'required'
        ];
    }
    protected function failedValidation(Validator $validator)
    {
        $this->baseFailedValidation($validator);
    }
}
