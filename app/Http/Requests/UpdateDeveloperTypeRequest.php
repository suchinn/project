<?php

namespace App\Http\Requests;

use App\Http\Requests\BaseRequest;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Contracts\Validation\Validator;
use Illuminate\Validation\ValidationException;
use Illuminate\Http\Exceptions\HttpResponseException;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;

class UpdateDeveloperTypeRequest extends BaseRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        // dd();
        return [
            'name' => 'required|unique:developer_types,name,'.$this->route('developer_type').',id,deleted_at,NULL',
            'status'=> 'regex:/^([1-2])$/',
        ];
    }

    protected function failedValidation(Validator $validator)
    {
        $this->baseFailedValidation($validator);
    }
}
