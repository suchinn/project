<?php

namespace App\Http\Requests;

use App\Http\Requests\BaseRequest;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Contracts\Validation\Validator;
use Illuminate\Validation\ValidationException;
use Illuminate\Http\Exceptions\HttpResponseException;
use Illuminate\Http\Request;


class CreatePositionRequest extends BaseRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name'          => 'required',
            'status'=> 'regex:/^([1-2])$/',
        ];
    }

    public function messages(){
        return [
            'status.regex' => trans('validation.custom.status_regex')
        ];
    }
    
    protected function failedValidation(Validator $validator)
    {
        $this->baseFailedValidation($validator);
    }
}
