<?php
use Tymon\JWTAuth\Facades\JWTAuth;
use Tymon\JWTAuth\Exceptions\JWTException;
use Tymon\JWTAuth\Exceptions\TokenInvalidException;
use Tymon\JWTAuth\Exceptions\TokenExpiredException;

function responseHelper($data, $status_code = 200){
	return response()->json([
		'status' => isset($data['status']) ? $data['status'] : true,
		'message' => isset($data['message']) ? $data['message'] : trans("errors.success"),
		'data' => isset($data['data']) ? $data['data'] : [],
	], $status_code );
}

function isSuperAdmin(){
	$userCurrent = JWTAuth::parseToken()->authenticate();
	return $userCurrent['role_id'] === 1 ? true : false;
}
function isAdmin(){
	$userCurrent = JWTAuth::parseToken()->authenticate();
	return $userCurrent['role_id'] === 2 ? true : false;
}

