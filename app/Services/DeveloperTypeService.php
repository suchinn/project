<?php
namespace App\Services;
use App\Repositories\Repository;
use App\Models\DeveloperType;

class DeveloperTypeService{
    private $model;

    public function __construct(DeveloperType $dev){
        $this->model = new Repository($dev);
    }
    /*
    * create a developer type
    * @param array $data
    * @return \Illuminate\Http\Response
    */
    public function createDeveloperType(array $data){
        if(!isset($data['status'])) $data['status'] = 1;
        $result = $this->model->store($data);
        return responseHelper([
            'data' => $result,
            'message' => trans('errors.success')
        ], HTTP_CREATED);
    }
    /*
    * edit a developer type
    * @param array $data, int $id
    * @return \Illuminate\Http\Response
    */
    public function updateDeveloperType($data,$id)
    {
        $skill = $this->model->getItemById($id);
        if(!$skill){
            return responseHelper([
                'message' => trans('errors.not_found'),
                'status' => false
            ], HTTP_NOT_FOUND);
        }
        $update = $this->model->update($data,$id);
        return responseHelper([
            'data' => $this->model->getItemById($id)
        ]);
    }
    /*
    * remove a developer type
    * @param int $id
    * @return \Illuminate\Http\Response
    */
    public function removeDeveloperType($id){
        $data= $this->model->getItemById($id);
        if(!$data){
            return responseHelper([
                'message'=> trans('errors.not_found'),
                'status'=> false
            ],HTTP_NOT_FOUND);
        }
        $this->model->destroy($id);
        return responseHelper([], HTTP_NO_CONTENT);
    }
    /*
    * show a developer type detail
    * @param int $id
    * @return \Illuminate\Http\Response
    */
    public function getDeveloperType($id){
        $data= $this->model->getItemByid($id);
        if(!$data){
            return responseHelper([
                'message'=>trans('errors.not_found'),
                'status'=>false
            ], HTTP_NOT_FOUND);
        }
        return responseHelper([
            'data' => $data,
            'message' => trans('errors.success')
        ]);
    }

    /*
    * show all developer type
    * @param array $request
    * @return \Illuminate\Http\Response
    */
    public function getAllDeveloperType()
    {
        $data = $this->model->getItems();
        return responseHelper([
            'data'=>$data
        ]);
    }
}
