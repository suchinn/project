<?php
namespace App\Services;

use App\Repositories\Repository;
use App\Models\Position;
use Illuminate\Support\Facades\Auth;
use App\Json;

class PositionService {
/*
* properties $model
* construct cho vui
 *
* */
    protected $model;
    public function __construct(Position $position)
    {
        $this->model = new Repository($position);
    }
    public function store(array $data)
    {
        $count = $this->model->count([
            'name' => $data['name']
        ]);
        if($count > 0){
            return responseHelper([
                'status' => false,
                'message' => trans('errors.duplicate_resource', ['attribute' => 'position name'])
            ], HTTP_INVALID);
        }
        $result = $this->model->store($data);
        return responseHelper([
            'data' => $result,
            'message' => trans('errors.created')
        ], HTTP_CREATED);
    }
/*
 *
 * remove position by id
 * @param $id
 * */
    public function delete($id)
    {
        $exist = $this->model->getItemById($id);
        if($exist=== null){
            return resposneHelper([
                'status' => false,
                'message' => trans('errors.not_found')
            ], HTTP_NOT_FOUND);
        }
        $this->model->destroy($id);
        return responseHelper([
            'data' => $exist,
        ], HTTP_NO_CONTENT);
    }
/*
 * show data by id
 * @param $id
 *
 *
 */
    public function show($id)
    {
        $data = $this->model->getItemById($id);
        if(!$data){
            return responseHelper([
                'message' => trans('errors.not_found'),
                'status' => false
            ], HTTP_NOT_FOUND);
        }
        return responseHelper([
            'data' => $data,
        ]);
    }
/*
 * update data from @hell
 *@param $id
 *
 * */
    public function update($data,$id)
    {
       $record = $this->model->getItemById($id);
        if(!$record){
            return responseHelper([
                'message' => trans('errors.not_found'),
                'status' => false
            ], HTTP_NOT_FOUND);
        }
        $existed = $this->model->existed('name',$data['name'],$id);
        if($existed){
            return responseHelper([
                'message' => trans('errors.duplicate_resource', ['attribute' => 'position name']),
                'status' => false
            ], HTTP_INVALID);
        }
        $row =  $this->model->update($data,$id);
        return responseHelper([
            'data' => $this->model->getItemById($id),
        ]);
    }
    public function index($request)
    {
        $searchTerm = array();
        $searchCols = ['name', 'description'];
        foreach($searchCols as $searchCol){
            if(array_key_exists($searchCol, $request)){
                $searchTerm[$searchCol] = [
                    'condition' => 'like',
                    'value' => "%{$request[$searchCol]}%"
                ];
            }
        }
        $result = $this->model->getAll($searchTerm);
        return responseHelper([
            'data' => $result
        ]);
    }
}
