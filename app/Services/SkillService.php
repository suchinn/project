<?php
namespace App\Services;
use App\Repositories\Repository;
use App\Models\Skill;

class SkillService{
    private $model;

    public function __construct(Skill $skill){
        $this->model = new Repository($skill);
    }
    /*
    * create a skill
    * @param array $data
    * @return \Illuminate\Http\Response
    */
    public function createSkill(array $data){
        if(!isset($data['status'])) $data['status'] = 1;
        $result = $this->model->store($data);
        return responseHelper([
            'data' => $result,
            'message' => trans('errors.success')
        ], HTTP_CREATED);
    }
    /*
    * edit a skill
    * @param array $data, int $id
    * @return \Illuminate\Http\Response
    */
    public function updateSkill($data,$id)
    {
        $skill = $this->model->getItemById($id);
        if(!$skill){
            return responseHelper([
                'message' => trans('errors.not_found'),
                'status' => false
            ], HTTP_NOT_FOUND);
        }
        $update = $this->model->update($data,$id);
        return responseHelper([
            'data' => $this->model->getItemById($id)
        ]);
    }
    /*
    * remove a skill
    * @param int $id
    * @return \Illuminate\Http\Response
    */
    public function removeSkill($id){
        $data= $this->model->getItemById($id);
        if(!$data){
            return responseHelper([
                'message'=> trans('errors.not_found'),
                'status'=> false
            ],HTTP_NOT_FOUND);
        }
        $this->model->destroy($id);
        return responseHelper([], HTTP_NO_CONTENT);
    }
    /*
    * show a skill detail
    * @param int $id
    * @return \Illuminate\Http\Response
    */
    public function getSkill($id){
        $data= $this->model->getItemByid($id);
        if(!$data){
            return responseHelper([
                'message'=>trans('errors.not_found'),
                'status'=>false
            ], HTTP_NOT_FOUND);
        }
        return responseHelper([
            'data' => $data,
            'message' => trans('errors.success')
        ]);
    }

    /*
    * show skill
    * @param array $request
    * @return \Illuminate\Http\Response
    */
    public function getAllSkill()
    {
        $data = $this->model->getItems();
        return responseHelper([
            'data'=>$data
        ]);
    }
}
