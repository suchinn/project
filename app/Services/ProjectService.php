<?php
namespace App\Services;

use App\Json;
use App\Repositories\Repository;
use App\Models\Project;
use App\Models\Staff;
use App\Models\Partner;
use App\Models\Staff_project;
use App\Http\Requests\ShowProjectRequest;
use Illuminate\Contracts\Support\Jsonable;

class ProjectService
{
    protected $model;
    public function __construct(Project $project)
    {
        $this->model = new Repository($project);
    }
    public function store($rq)
    {
        $staff = Staff::pluck('id')->toArray();
        $partner = Partner::pluck('id')->toArray();
        $rq['start_date'] = date('Y-m-d',strtotime($rq['start_date']));
        $rq['end_date'] = date('Y-m-d',strtotime($rq['end_date']));
        if(array_key_exists($rq['project_type_id'],PROJECT_TYPE)==false){
            return Json::response(false,HTTP_NOT_FOUND,trans('errors.not_exist',['attribute'=>'Project type']));
        }
        if(array_key_exists($rq['status_id'],PROJECT_STATUS)==false){
            return Json::response(false,HTTP_NOT_FOUND,trans('errors.not_exist',['attribute'=>'Project status']));
        }
        if(in_array($rq['brse_id'],$staff)==false){
            return Json::response(false,HTTP_NOT_FOUND,trans('errors.not_exist',['attribute'=>'Brse']));
        }
        if(in_array($rq['comtor_id'],$staff)==false){
            return Json::response(false,HTTP_NOT_FOUND,trans('errors.not_exist',['attribute'=>'Comtor']));
        }
        if(array_key_exists($rq['leader_id'],POSITION)==false){
            return Json::response(false,HTTP_NOT_FOUND,trans('errors.not_exist',['attribute'=>'Leader ']));
        }
        if(array_key_exists($rq['team_leader_id'],POSITION)==false){
            return Json::response(false,HTTP_NOT_FOUND,trans('errors.not_exist',['attribute'=>'Team leader ']));
        }
        if(in_array($rq['partner_id'],$partner)==false){
            return Json::response(false,HTTP_NOT_FOUND,trans('errors.not_exist',['attribute'=>'Partner']));
        }
        $project_code = $this->model->existed('project_code',$rq['project_code'],null);
        if($project_code){
            return Json::response(false,HTTP_NOT_FOUND,trans('errors.taken',['attribute'=>'project code']));
        }
        $project_name = $this->model->existed('name',$rq['name'],null);
        if($project_name){
            return Json::response(false,HTTP_NOT_FOUND,trans('errors.taken',['attribute'=>'project name']));
        }
        $project = $this->model->store($rq);
        return Json::response(true,HTTP_OK,trans('errors.success'),$rq);
    }
    public function show($id)
    {
        $project = $this->model->getItemById($id);
        if(!$project){
            return Json::response(false,HTTP_NOT_FOUND,trans('errors.not_found'));
        }
        return Json::response(true,HTTP_OK,trans('errors.success'),$project);
    }
    public function destroy($id)
    {
        $project = $this->model->getItemById($id);
        if(!$project){
            return Json::response(false,HTTP_NOT_FOUND,trans('errors.not_found'));
        }
        $relationship = Staff_project::where('project_id',$id);
        $relationship->delete();
        $this->model->destroy($id);
        return Json::response(true,HTTP_OK,trans('errors.success'),$project);
    }
    public function index($request,$page,$limit)
    {
        $records = Project::paginate(10);
//        $data =  $records->limit($limit)->offset(($page - 1) * $limit)->get();
        return Json::response(true,HTTP_OK,trans('errors.success'),[$records]);
    }

    public function update($data,$id)
    {
        $record = $this->model->getItemById($id);
        if(!$record){
            return Json::response(false,HTTP_NOT_FOUND,trans('errors.not_found'));
        }
        $name = $this->model->existed('name',$data['name'],$id);
        if($name){
            return Json::response(false,HTTP_NOT_FOUND,trans('errors.taken',['attribute'=>'name']));
        }
        $project_code = $this->model->existed('project_code',$data['project_code'],$id);
        if($project_code){
            return Json::response(false,HTTP_NOT_FOUND,trans('errors.taken',['attribute'=>'Project code']));
        }
        $this->model->update($data,$id);
        return Json::response(true,HTTP_OK,trans('errors.success'),$data);
    }
}
