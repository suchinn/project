<?php
namespace App\Services;
use App\Mail\SendMail;
use Illuminate\Contracts\Notifications\Dispatcher;
use Tymon\JWTAuth\Facades\JWTAuth;
use Illuminate\Support\Facades\Mail;
use Illuminate\Http\Request;
use Illuminate\Bus\Queueable;
use Illuminate\Notifications\Notification;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Support\Facades\Auth;
use App\Notifications\PasswordResetRequest;
use App\Notifications\PasswordResetSuccess;
use Illuminate\Notifications\Notifiable;
use App\Models\PasswordReset;
use Illuminate\Support\Str;
use Carbon\Carbon;
use App\Models\User;
use App\Json;
use Hash;

class AuthService {
    use Notifiable;
	private $user;
	public function __construct(User $user)
	{
	    $this->user = $user;
	}
    // xử lý đăng nhap
    public function handleLogin(array $credentials)
    {
        if ($token = auth('api')->attempt($credentials)){
            return Json::response(true,HTTP_OK,trans('errors.login_success'),[
                'access_token' => $token,
                'token_type'   => 'bearer',
                'expires_in'   => auth('api')->factory()->getTTL() * EXPIRE_TOKEN,
            ]);
        }
        return Json::response(false,HTTP_UNAUTHORIZED,trans('auth.failed'));
    }
	public function handleLogout()
	{
       try{
            Auth::guard('api')->logout();
             return Json::response(true,HTTP_OK,'logout');
       }catch(\Exception $e){
           return Json::response(false,HTTP_INVALID,trans('password.token'));
       }
	}
    public function guard()
    {
        return Auth::guard();
    }
    public function ResetPassword($email)
    {
        $user = User::where('email', $email)->first();
        if (!$user){
            return Json::response(false,HTTP_NOT_FOUND,trans('passwords.user'));
        }
        $passwordReset = PasswordReset::updateOrCreate(
            ['email'        => $user->email],
            [
                'user_id'   => $user->id,
                'email'     => $user->email,
                'token'     => Str::random(60)
            ]
        );
        if ($user && $passwordReset){
            $user->notify(
                new PasswordResetRequest($passwordReset->email,$passwordReset->token)
            );
        }
        return Json::response(true,HTTP_OK,trans('passwords.sent'));
    }
    public function Confirm($token)
    {
        $passwordReset = PasswordReset::where('token', $token)
            ->first();
        if (!$passwordReset){
            return Json::response(false,HTTP_NOT_FOUND,trans('passwords.token'));
        }
        if (Carbon::parse($passwordReset->updated_at)->addMinutes(EXPIRE_TOKEN)->isPast()) {
            $passwordReset->delete();
            return Json::response(false,HTTP_NOT_FOUND,trans('passwords.token'));
        }
        return Json::response(true,HTTP_OK,trans('passwords.sent'), ['token' => $passwordReset->token]);
    }
     public function Reset($passwordReset,$newPassword)
    {
        if (!$passwordReset){
            return Json::response(false,HTTP_NOT_FOUND,trans('passwords.token'));
        }
        $user = User::where('email', $passwordReset->email)->first();
        if (!$user) {
            return Json::response(false,HTTP_NOT_FOUND,trans('passwords.user'));
        }
        $user->password = bcrypt($newPassword);
        $user->save();
        $passwordReset->delete();
        $user->notify(new PasswordResetSuccess($passwordReset));
        return responseHelper([
            'data' => $user,
        ]);
    }
}
