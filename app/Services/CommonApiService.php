<?php
namespace App\Services;
use App\Repositories\Repository;
use App\Models\Group;
use App\Models\Partner;
use App\Models\Rank;
use App\Models\DeveloperType;
use App\Models\Skill;

use App\Json;

class CommonApiService{

    public function common(){
        $groups = [];
        $ranks = [];
        $developerTypes = [];
        $skills = [];
        $partners = [];
        
        foreach (Group::all() as $group){
            $groups[$group->id] = $group;
        }
        foreach (Partner::all() as $partner){
            $partners[$partner->id] = $partner;
        }
        foreach (Rank::all() as $rank){
            $ranks[$rank->id] = $rank;
        }
        foreach (DeveloperType::all() as $devType){
            $developerTypes[$devType->id] = $devType;
        }
        foreach (Skill::all() as $skill){
            $skills[$skill->id] = $skill;
        }
       
        $data = [
            'rank' => $ranks,
            'position' => POSITION,
            'group' => $groups,
            'developer_type' => $developerTypes,
            'skill' => $skills,
            'language' => LANGUAGE,
            'working_type' => WORK_TYPE,
            'partner' => $partners   
        ];
        if(isSuperAdmin()){
            $data['project_type'] = PROJECT_TYPE;
            $data['project_status'] = PROJECT_STATUS;
        }
        return \responseHelper([
            'data' => $data
        ]);
    }

}
