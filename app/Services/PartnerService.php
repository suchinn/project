<?php
namespace App\Services;
use App\Repositories\Repository;
use App\Models\Partner;

class PartnerService{
    private $model;
    /*
    * constructor PartnerService
    * @param Partner $partner
    * @return Repository
    */
    public function __construct(Partner $partner){
        $this->model = new Repository($partner);
    }
    /*
    * create new a partner
    * @param array $partner
    * @return \Illuminate\Http\Response
    */
    public function createPartner($partner){
        $partnerByName = $this->model->count([
            'name' => $partner['name']
        ]);
        if($partnerByName > 0){
            return responseHelper([
                'status' => false,
                'message' => trans('errors.duplicate_resource', ['attribute' => 'partner name'])
            ], HTTP_INVALID);
        }

        if(!isset($partner['status'])) $partner['status'] = 1;
        $result = $this->model->store($partner);
        return \responseHelper([
            'data' => $result,
            'message' => trans('errors.success')
        ], HTTP_CREATED);
    }
    /*
    * get a partner detail
    * @param int $id
    * @return \Illuminate\Http\Response
    */
    public function getPartner($id){
        $partner = $this->model->getItemById($id);
        if(!$partner){
            return responseHelper([
                'message' => trans('errors.not_found'),
                'status' => false
            ], HTTP_NOT_FOUND);
        }
        return responseHelper([
            'data' => $partner,
            'message' => trans('errors.success')
        ]);
    }
    /*
    * update a partner
    * @param array $data, int $id
    * @return \Illuminate\Http\Response
    */
    public function updatePartner($data, $id){
        $partner = $this->model->getItemById($id);
        if(!$partner){
            return responseHelper([
                'message' => trans('errors.not_found'),
                'status' => false
            ], HTTP_NOT_FOUND);
        }
        if(isset($data['name'])){
            $existed = $this->model->existed('name', $data['name'], $id);
            if($existed > 0){
                return responseHelper([
                    'status' => false,
                    'message' => trans('errors.duplicate_resource', ['attribute' => 'partner name'])
                ],HTTP_INVALID);
            }
        }

        $result = $this->model->update($data, $id);
        return responseHelper([
            'data' => $this->model->getItemById($id),
            'message' => trans('errors.success')
        ]);
    }
    /*
    * remove a partner use soft delete
    * @param int $id
    * @return \Illuminate\Http\Response
    */
    public function removePartner($id){
        $partner = $this->model->getItemById($id);
        if(!$partner){
            return responseHelper([
                'code' => HTTP_NOT_FOUND,
                'message' => trans('errors.not_found'),
                'status' => false
            ],HTTP_NOT_FOUND);
        }
        $result = $this->model->destroy($id);
        return responseHelper([], HTTP_NO_CONTENT);
    }
    /*
    * display partner list
    * @param array $request
    * @return \Illuminate\Http\Response
    */
    public function getAllPartner($request){
        $searchTerm = array();
        $searchCols = ['name', 'description'];
        foreach($searchCols as $searchCol){
            if(array_key_exists($searchCol, $request)){
                $searchTerm[$searchCol] = [
                    'condition' => 'like',
                    'value' => "%{$request[$searchCol]}%"
                ];
            }
        }
        $result = $this->model->getAll($searchTerm);
        return responseHelper([
            'data' => $result,
            'code' => HTTP_OK
        ]);
    }
}
