<?php
namespace App\Services;
use App\Repositories\Repository;
use App\Models\Rank;

class RankService{
    private $model;
    /*
    * constructor RankService
    * @param Rank $rank
    * @return Repository
    */

    public function __construct(Rank $rank){
        $this->model = new Repository($rank);
    }
    /*
    * create a rank
    * @param array $data
    * @return \Illuminate\Http\Response
    */
    public function store(array $data){
        if(!isset($data['status'])) $data['status'] = 1;
        $result = $this->model->store($data);
        return responseHelper([
            'data' => $result,
            'message' => trans('errors.success')
        ], HTTP_CREATED);
    }
    /*
    * edit a rank
    * @param array $data, int $id
    * @return \Illuminate\Http\Response
    */
    public function update($data,$id)
    {
        $rank = $this->model->getItemById($id);
        if(!$rank){
            return responseHelper([
                'message' => trans('errors.not_found'),
                'status' => false
            ], HTTP_NOT_FOUND);
        }
        $row = $this->model->update($data,$id);
        return responseHelper([
            'data' => $this->model->getItemById($id)
        ]);
    }
    /*
    * remove a partner
    * @param int $id
    * @return \Illuminate\Http\Response
    */
    public function destroy($id){
        $data= $this->model->getItemById($id);
        if(!$data){
            return responseHelper([
                'message'=> trans('errors.not_found'),
                'status'=> false
            ],HTTP_NOT_FOUND);
        }
        $this->model->destroy($id);
        return responseHelper([
            'data'=> $id,
        ], HTTP_NO_CONTENT);
    }
    /*
    * show a rank detail
    * @param int $id
    * @return \Illuminate\Http\Response
    */
    public function show($id){
        $data= $this->model->getItemByid($id);
        if(!$data){
            return responseHelper([
                'message'=>trans('errors.not_found'),
                'status'=>false
            ], HTTP_NOT_FOUND);
        }
        return responseHelper([
            'data' => $data,
            'message' => trans('errors.success')
        ]);
    }

    /*
    * show rank
    * @param array $request
    * @return \Illuminate\Http\Response
    */
    public function getAllRank($request)
    {
        $searchTerm = array();
        $searchCols = ['name','description'];
        foreach($searchCols as $searchCol){
            if(array_key_exists($searchCol,$request)){
                $searchTerm[$searchCol]=[
                    'condition'=>'like',
                    'value'=>"%{$request[$searchCol]}%"
                ];
            }
        }
        $request = $this->model->getAll($searchTerm);
        return responseHelper([
            'data'=>$request
        ]);
    }
}
