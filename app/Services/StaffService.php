<?php
namespace App\Services;
use App\Json;
use App\Repositories\Repository;
use App\Models\Staff;
use App\Models\DeveloperType;
use App\Models\Rank;
use App\Models\Position;
use App\Models\Group;
use App\Models\Skill;
use App\Models\StaffSkill;
use App\Models\Language;
use DB;

class StaffService{
    protected $model;
    public function __construct(Staff $staff){
        $this->model = new Repository($staff);
    }
    public function createStaff($request){
        try {
            $staff_id = null;
            DB::transaction(function () use ($request, &$staff_id) {
                //insert staff
                if(array_key_exists('experience_from', $request)){
                    $request['experience_from'] = date("Y/m/d", strtotime($request['experience_from']));;
                }
                if(!array_key_exists('status', $request)){
                    $request['status'] = 1;
                }
                $staff = $this->model->store($request);
                $staff_id = $staff['id'];
                //insert language
                if(array_key_exists('language_code', $request)){
                    $insertLang = [];
                    foreach($request['language_code'] as $code){
                        array_push($insertLang, [
                            'language_code' => $code,
                            'staff_id' => $staff_id,
                        ]);
                    }
                    Language::insert($insertLang);
                }
                if(array_key_exists('skill_id', $request)){
                    $insertSkill = [];
                    foreach($request['skill_id'] as $skill_id){
                        array_push($insertSkill, [
                            'skill_id' => $skill_id,
                            'staff_id' => $staff_id,
                            'status' => 1 ,//temp
                            'created_at' => DB::raw('CURRENT_TIMESTAMP'),
                            'updated_at' => DB::raw('CURRENT_TIMESTAMP')
                        ]);
                    }
                    StaffSkill::insert($insertSkill);
                }
            });

            $staff = $this->model->with('languages')->with('staff_skill')->find($staff_id);
            $staff = $this->formatData($staff);
            return responseHelper(['data' => $staff], HTTP_CREATED);
        } catch (\Throwable $th) {
            return responseHelper([
                        'message' => trans('errors.server_error'),
                        'status' => false
                    ], HTTP_SERVER_ERROR);
        }

    }

    public function removeStaff($id){
        $data= $this->model->getItemById($id);
        if(!$data){
            return responseHelper([
                'message'=> trans('errors.not_found'),
                'success'=> false
            ],HTTP_NOT_FOUND);
        }
        $this->model->destroy($id);
        return responseHelper([
            'data'=> $data,
            'code'=> HTTP_OK
        ]);
    }

    public function getAllStaff($request){
        $searchTerm = [];
        $searchCols = ['full_name'];
        foreach($searchCols as $searchCol){
            if(array_key_exists($searchCol, $request)){
                $searchTerm[$searchCol] = [
                    'condition' => 'like',
                    'value' => "%{$request[$searchCol]}%"
                ];
            }
        }
        $limit = null;
        $offset = null;
        if(isset($request['limit']) && isset($request['page'])){
            $limit = $request['limit'];
            $offset = ($request['page'] - 1) * $limit;
            if($offset < 0) $offset = 0;
        }
        $staffList = $this->model->getAll($searchTerm, $offset, $limit, ['languages', 'staff_skill']);
        $result = [];
        // foreach($staffList as $staff){
        //     array_push($result, $this->formatData($staff));
        // }
        $result['staffs'] = [];
        $result['count'] = 0;
        foreach($staffList as $staff){
            array_push($result['staffs'], $this->formatData($staff));
        }
        $totalRecord = $this->model->getAll($searchTerm)->count();
        $result['count'] = $totalRecord;
        return responseHelper([
            'data' => $result,
        ]);
    }

    public function formatData($staff){
        $staff = $staff->toArray();
        $staff['language_code'] = array();
        $staff['skill_id'] = array();
        foreach($staff['languages'] as $lang){
            array_push($staff['language_code'], $lang['language_code']);
        }
        foreach($staff['staff_skill'] as $skill){
            array_push($staff['skill_id'], $skill['skill_id']);
        }
        unset($staff['languages'], $staff['staff_skill']);
        return $staff;
    }
    public function checkExistRecord($data){
        // $data=['developer_type_id' => $id1, 'group_id' => $id2, 'position_id' => $id3, 'rank_id' => $id4];
        if(array_key_exists('developer_type_id',$data)){
            $devType = DeveloperType::find($data['developer_type_id']);
            if(!$devType) return false;
        }
        if(array_key_exists('group_id',$data)){
            $group = Group::find($data['group_id']);
            if(!$group) return false;
        }
        if(array_key_exists('position_id',$data)){
            $position = Position::find($data['position_id']);
            if(!$position) return false;
        }
        if(array_key_exists('rank_id',$data)){
            $rank = Rank::find($data['rank_id']);
            if(!$rank) return false;
        }
        if(array_key_exists('skill_id', $data)){
            //get all skill id from skill model
            $skillIds = Skill::pluck('id')->toArray();
            if(count(array_intersect($skillIds, $data['skill_id'])) != count($data['skill_id'])) return false;
        }
        if(array_key_exists('language_id', $data)){
            //get all temporary language id from constant
            $langs = LANGS;
            $langIds = array_map(function($lang){
                return $lang['id'];
            }, $langs);
            if(count(array_intersect($langIds, $data['language_id'])) != count($data['language_id'])) return false;
        }
        if(array_key_exists('working_type_id', $data)){
            //get all temporary working type id from constant
            $workingTypes = WORKING_TYPE;
            $workingTypeIds = array_map(function($workingType){
                return $workingType['id'];
            }, $workingTypes);
            if(!in_array($data['working_type_id'], $workingTypeIds)) return false;
        }
        return true;

    }

    public function show($id)
    {
        $record = $this->formatData($this->model->with('languages')->with('staff_skill')->find($id));
        if (!$record){
            return Json::response(false,HTTP_NOT_FOUND,trans('errors.not_found'));
        }
        return Json::response(true,HTTP_OK,trans('errors.success'),$record);
    }
    
    public function updateStaff($request, $id){
        $data= $this->model->getItemById($id);
        if(!$data){
            return responseHelper([
                'message'=> trans('errors.not_found'),
                'success'=> false
            ],HTTP_NOT_FOUND);
        }
        //only for reason resign
        if(isset($request['note'])){
            $this->model->update([
                'note' => $request['note'],
                'status' => 3, //temp
            ], $id);
            return responseHelper([
                'data' => $this->model->with('languages')->with('staff_skill')->find($id)
                ]);
        }
        try {
            DB::transaction(function () use ($request, $id) {
                //update staff
                if(array_key_exists('experience_from', $request)){
                    // $request['experience_from'] = date("Y/m/d", strtotime($request['experience_from']));
                    // dd(strtotime($request['experience_from']));
                    $date = (date_create_from_format('d/m/Y', $request['experience_from']));
                    $request['experience_from'] = date_format($date, 'Y-m-d');
                }
                $staff = $this->model->update($request, $id);
                //update language
                if(array_key_exists('language_code', $request)){
                    $langDB = Language::select('language_code')->where('staff_id', $id)->get();
                    $langDB = array_map(function($value){
                        return $value['language_code'];
                    }, $langDB->toArray());

                    if(count($request['language_code']) == 0){
                        //delete overall
                        foreach($langDB as $code){
                            Language::where('language_code', $code)->where('staff_id', $id)
                                    ->delete();
                        }
                    }
                    else{
                        foreach($request['language_code'] as $code){
                            if(!in_array($code, $langDB)){
                                //insert
                                $d = Language::create([
                                    'language_code'=> $code, 
                                    'staff_id' => $id
                                    ]);
                            }
                        }
                        foreach($langDB as $code){
                            if(!in_array($code, $request['language_code'])){
                                //delete
                                Language::where('language_code', $code)->where('staff_id', $id)
                                        ->delete();
                            }
                        }
                    }
                }
                if(array_key_exists('skill_id', $request)){
                    $skillDB = StaffSkill::select('skill_id')->where('staff_id', $id)->get();
                    $skillDB = array_map(function($value){
                        return $value['skill_id'];
                    }, $skillDB->toArray());
                    if(count($request['skill_id']) == 0){
                        //delete overall
                        foreach($skillDB as $skill){
                            StaffSkill::where('skill_id', $skill)->where('staff_id', $id)
                                    ->delete();
                        }
                    }
                    else{
                        foreach($request['skill_id'] as $skill){
                            if(!in_array($skill, $skillDB)){
                                //insert
                                StaffSkill::create([
                                    'skill_id'=> $skill, 
                                    'staff_id' => $id, 
                                    'status' => 1 ,//temp
                                    'created_at' => DB::raw('CURRENT_TIMESTAMP'),
                                    'updated_at' => DB::raw('CURRENT_TIMESTAMP')
                                ]);
                            }
                        }
                        foreach($skillDB as $skill){
                            if(!in_array($skill, $request['skill_id'])){
                                //delete
                                StaffSkill::where('skill_id', $code)->where('staff_id', $id)
                                        ->delete();
                            }
                        }
                    }
                }
            });
            
            $staff = $this->model->with('languages')->with('staff_skill')->find($id);
            $staff = $this->formatData($staff);
            return responseHelper(['data' => $staff]);
        } catch (\Throwable $th) {
            return responseHelper([
                        'message' => trans('errors.server_error'),
                        'status' => false
                    ], HTTP_SERVER_ERROR);
        }
    }
}
