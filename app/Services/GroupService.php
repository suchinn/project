<?php
namespace App\Services;
use App\Repositories\Repository;
use App\Models\Group;

class GroupService{
    private $model;
    public function __construct(Group $group){
        $this->model = new Repository($group);
    }
    public function detailGroup($id){
        $group = $this->model->getItemById($id);
        if(!$group){
            return responseHelper([
                'message' => trans('errors.not_found'),
                'status' => false
            ], HTTP_NOT_FOUND);
        }
        return responseHelper([
            'data' => $group,
        ]);
    }

    public function updateGroup($data, $id){
        $group = $this->model->getItemById($id);
        if(!$group){
            return responseHelper([
                'message' => trans('errors.not_found'),
                'status' => false
            ], HTTP_NOT_FOUND);
        }

        if(isset($data['name'])){
            $existed = $this->model->existed('name', $data['name'], $id);
            if($existed > 0){
                return responseHelper([
                    'status' => false,
                    'message' => trans('errors.duplicate_resource', ['attribute' => 'group name'])
                ], HTTP_INVALID);
            }
        }

        $result = $this->model->update($data, $id);

        return responseHelper([
            'data' => $this->model->getItemById($id),
            'code' => HTTP_OK,
        ]);
    }

    public function getAllGroup($request){
        $searchTerm = array();
        $searchCols = ['name', 'description'];
        foreach($searchCols as $searchCol){
            if(array_key_exists($searchCol, $request)){
                $searchTerm[$searchCol] = [
                    'condition' => 'like',
                    'value' => "%{$request[$searchCol]}%"
                ];
            }
        }
        $result = $this->model->getAll($searchTerm);
        return responseHelper([
            'data' => $result,
            'code' => HTTP_OK,
        ]);
    }


    public function store(array $data){
        //return $this->model->store($data);
        $groupByName= $this->model->count([
            'name' => $data['name']
        ]);
        if($groupByName > 0){
            return responseHelper([
                'status' => false,
                'message' => trans('errors.duplicate_resource', ['attribute' => 'group name'])
            ],HTTP_INVALID);
        }

        if(!isset($data['status'])) $data['status'] = 1;
        $result = $this->model->store($data);
        return responseHelper([
            'data' => $result,
        ], HTTP_CREATED);
    }

    /*
    * delete group by id
    *@param int $id
    *@return array
    */
    public function destroy($id){
        $data = $this->model->getItemById($id);
        if(!$data){
            return responseHelper([
                'message' => trans('errors.not_found'),
                'status' => false
            ],HTTP_NOT_FOUND);
        }
        $this->model->destroy($id);
        return responseHelper([],HTTP_NO_CONTENT);
    }
}
