<?php
namespace App\Repositories;

use Illuminate\Database\Eloquent\Model;
use App\Repositories\RepositoryInterFace;

class Repository implements RepositoryInterFace {
	protected $model;
	public function __construct(Model $model)
	{
	    return $this->model = $model;
	}
	public function getItems()
	{
	    return $this->model->all();
	}
	public function store(array $data)
	{
	    return $this->model->create($data);
	}
	public function update(array $data, $id)
	{
		$record = $this->model->find($id);
	    return $record->update($data);
	}
	public function destroy($id)
	{
	    return $this->model->destroy($id);
	}
	public function getItemById($id)
	{
	    return $this->model->find($id);
	}
	  public function setModel($model)
    {
        $this->model = $model;
        return $this;
    }
    public function with($relations)
    {
        return $this->model->with($relations);
	}
	public function getAll($searchTerm=null, $offset=null, $limit=null, $relations=null){
		$result = $this->model->query();
		if($searchTerm!=null){
            foreach($searchTerm as $key=>$term){
				$result = $result->orWhere($key, $term['condition'], "{$term['value']}");
			}
		}
		if($relations){
			$result = $result->with($relations);
		}

		if($limit!==null && $offset!==null){
			$result = $result->offset($offset)->limit($limit);
		}
		
		return $result->get();

	}
	/*
    * count amount reocord
    * @param array $search_term
    * @return int
    */
	public function count($searchTerm=null){
		$result = $this->model->query();
		if($searchTerm!=null){
            foreach($searchTerm as $key=>$term){
				$result = $result->where($key, $term);
			}
		}
		return $result->count();
	}

    /*
     *check existed column 
     * @param $model
     * @param $column
     * @param $value
     *
     * */
    function existed($column,$value,$id){
        $data = $this->model->where($column,$value)->where('id','!=',$id)->count();
        if($data){
            return true;
        }
        return false;
    }
}
