<?php
namespace App\repositories;

interface RepositoryInterFace {

	public function getItems();
	public function store(array $data);
	public function update(array $data, $id);
	public function getItemById($id);
	public function destroy($id);

}
