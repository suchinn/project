<?php
namespace App\Repositories;

use App\Repositories\Repository;
use App\Models\Group;

class RepositoryGroup extends Repository {
	protected $model;
	public function __construct(Group $group)
	{
	    return $this->model = $group;
    }
}