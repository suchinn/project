<?php

namespace App\Exceptions;

use Exception;
use Illuminate\Foundation\Exceptions\Handler as ExceptionHandler;
use Symfony\Component\HttpKernel\Exception\MethodNotAllowedHttpException;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

class Handler extends ExceptionHandler
{
    /**
     * A list of the exception types that are not reported.
     *
     * @var array
     */
    protected $dontReport = [
        //
    ];

    /**
     * A list of the inputs that are never flashed for validation exceptions.
     *
     * @var array
     */
    protected $dontFlash = [
        'password',
        'password_confirmation',
    ];

    /**
     * Report or log an exception.
     *
     * @param  \Exception  $exception
     * @return void
     */
    public function report(Exception $exception)
    {
        parent::report($exception);
    }

    /**
     * Render an exception into an HTTP response.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Exception  $exception
     * @return \Illuminate\Http\Response
     */
    public function render($request, Exception $exception)
    {
        if ($exception instanceof MethodNotAllowedHttpException)
        {
            return responseHelper([
                'status' => false,
                'message' => trans('errors.method_not_allow'),
            ], HTTP_METHOD_NOT_ALLOW);
        }
        if ($exception instanceof NotFoundHttpException ) {
            return responseHelper([
                'status'       => false,
                'message'       => 'Page Not Found!'
            ], HTTP_NOT_FOUND);
        }
        if ($exception instanceof \Illuminate\Session\TokenMismatchException) {
            return responseHelper([
                'status'       => false,
                'message'       => trans('errors.expired'),
            ], HTTP_EXPIRED);
        }
        return parent::render($request, $exception);
    }
}
