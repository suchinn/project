<?php

namespace App\Imports;

use App\Models\Rank;
use Maatwebsite\Excel\Concerns\ToModel;

class RanksImport implements ToModel
{
    /**
    * @param array $row
    *
     *  @return \Illuminate\Database\Eloquent\Model|null
    */
    public function model(array $row)
    {
        return new Rank([
            'name'=> $row[0],
            'status'=>$row[1],
            'description'=> $row[2],
        ]);
    }
}
