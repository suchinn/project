<?php

namespace App\Imports;
use App\Models\DeveloperType;
use Maatwebsite\Excel\Concerns\ToModel;

class DeveloperTypeImport  implements ToModel
{
    public function model(array $row)
    {
        return new DeveloperType([
            'name'     => $row[0],
            'status'    => $row[1],
            'description'=> $row[2],
        ]);
    }
}
