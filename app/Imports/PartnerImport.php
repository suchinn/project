<?php

namespace App\Imports;

use App\Models\Partner;
use Maatwebsite\Excel\Concerns\ToModel;

class PartnerImport implements ToModel
{
    /**
    * @param array $row
    *
    * @return \Illuminate\Database\Eloquent\Model|null
    */
    public function model(array $row)
    {
        return new Partner([
            'name'=>$row[0],
            'description'=>$row[1],
            'status'=>$row[2],
        ]);
    }
}
