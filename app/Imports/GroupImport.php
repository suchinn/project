<?php

namespace App\Imports;

use App\Models\Group;
use Maatwebsite\Excel\Concerns\ToModel;

class GroupImport implements ToModel
{
    /**
    * @param array $row
    *
    * @return \Illuminate\Database\Eloquent\Model|null
    */
    public function model(array $row)
    {
        return new Group([
            'name'=> $row[0],
            'description'=> $row[1],
            'status'=> $row[2],
            'leader_id'=> $row[3]
        ]);
    }
}
