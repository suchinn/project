<?php
return [
    'not_exist'=>'The :attribute not exist.',
    'taken' => 'You :attribute already taken',
    'success' => 'Successful!',
    'not_found' => 'Record not found',
    'forbidden' => 'You can not access this server',
    'unauthorized' => 'unauthorized',
    'no_content' => 'no content',
    'created' => 'Created successfully',
    'server_error' => 'Internal Server Error',
    'invalid' =>'invalid  request',
    'server_busy'=>'Server Busy please try again later.',
    'Conflict' => 'Conflict',
    'duplicate_resource' => 'Duplicate :attribute',
    'expired'   => 'session expired',
    'method_not_allow'=> 'Method is not allowed for the requested route',
    'login_success'     => 'Logged in successfully',
];
