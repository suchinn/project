<?php
const LANGS = [
    [
        'id' => 1,
        'code' => 'en',
        'value' => 'English'
    ],
    [
        'id' => 2,
        'code' => 'vi',
        'value' => 'Viet Nam'
    ],
    [
        'id' => 3,
        'code' => 'zh',
        'value' => 'Chinese'
    ],
    [
        'id' => 4,
        'code' => 'fr',
        'value' => 'French'
    ],
    [
        'id' => 5,
        'code' => 'ru',
        'value' => 'Russian'
    ]
];